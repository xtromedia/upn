<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xm
{
  	protected 	$ci;
  	var $do_icon = array(
					"insert" => "<i class='icon-pencil'></i>",
					"detail" => "<i class='icon-tasks'></i>",
					"edit"   => "<i class='icon-edit'></i>",
					"delete" => "<i class='icon-remove'></i>"
				);
	var $view_output = array(
						"modal"          => "show-modal",
						"portlet-after"  => "show-portlet portlet-after",
						"portlet-before" => "show-portlet portlet-before"
					);
	var $button_color = array(
						"insert" => "blue",
						"detail" => "blue",
						"edit"   => "yellow",
						"delete" => "red"
					);
	var $href;
	var $class;
	var $output_action;
	var $output_title;
	var $trigger_icon;
	var $trigger_do;
	var $field_elemen;
	var $field_label;
	var $field_validation;
	var $field_template = "	<div class='control-group'>
                				<label class='control-label'>{label_place}</label>
                				<div class='controls'>
                					{field_place}
                				</div>
            				</div>";

    var $input_template = "<input type='{type}' name='{name}' id='{id}' class='{class}' value='{value}'/>";

    var $datepicker_template = "<div class='{class}' data-date='{value}' data-date-format='{format}' data-date-viewmode='years'><input class='m-wrap m-ctrl-medium' name='{name}' id='{id}' type='text' value='{value}' /><span class='add-on'><i class='icon-calendar'></i></span></div>";

    var $input_file_upload_template = "<div class='{class}' data-provides='fileupload'>
			<div class='input-append'>
				<div class='uneditable-input'>
					<i class='icon-file fileupload-exists'></i> 
					<span class='fileupload-preview'></span>
				</div>
				<span class='btn btn-file'>
				<span class='fileupload-new'>Select file</span>
				<span class='fileupload-exists'>Change</span>
				<input type='file' name='{name}' id='{id}' class='default' />
				</span>
				<a href='#' class='btn fileupload-exists' data-dismiss='fileupload'>Remove</a>
			</div>
		</div>";

	var $image_upload_template = "<div class='{class}' data-provides='fileupload'>
		<div class='fileupload-new thumbnail' style='width: 200px; height: 150px;'>
			<img src='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' alt=' />
		</div>
		<div class='fileupload-preview fileupload-exists thumbnail' style='max-width: 200px; max-height: 150px; line-height: 20px;'></div>
		<div>
			<span class='btn btn-file'><span class='fileupload-new'>Select image</span>
			<span class='fileupload-exists'>Change</span>
			<input type='file' name='{name}' id='{id}' class='default' /></span>
			<a href='#' class='btn fileupload-exists' data-dismiss='fileupload'>Remove</a>
		</div>
	</div>";

	var $select_template = "<select class='{class}' data-placeholder='{placeholder}' name='{name}' id='{id}'>
        						<option value=''>{placeholder}</option>
        						{option_place}
        					</select>";

    var $option_template = "<option value='{option_value}' {selected}>{option_name}</option>";  

    var $radio_template  = "<label class='{class}'>
								<input type='radio' name='{option_name}' id='{option_name}-{option_value}' value='{option_value}' {selected}/>
								{radio_label}
							</label>";	

	var $textarea_template = "<textarea class='{class}' name='{name}' id='{id}' rows='5'>{value}</textarea>";						

	var $field_text = "<span class='text'>{text}</span>";

	var $form_text = "<div class='form-horizontal form-view form-bordered form-label-stripped scroll-container'>";
	var $table_open = "<table class='table table-striped table-bordered table-hover flip-content data-tables-handler' id='{id}'>";
	var $table_close = "</table>";

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	//merubah nominal minus menjadi nominal dalam kurung sesuai format standar accounting
	// contoh -20.000 menjadi (20.000)
	function format_accounting($number,$profix=0) {
		$amount = number_format($number, $profix);
	    if ($amount < 0) {
	    	$amount = str_replace("-","", $amount);
	    	return '(' . $amount . ')';
	    }else{
	    	return $amount;
	    }	
	}

	//merubah format tanggal dalam format apapun ke format yang diinginkan
	// defaultnya berubah ke format contoh : 04 aug 2013
	function format_tanggal($obj, $format="d M Y"){
		$data = date_format(date_create($obj), $format);
		return $data;
	}

	//function untuk merubah angka ke bilangan romawi
	function convertToRomawi($month){
		$romawi = array('01' => 'I',
						'02' => 'II',
						'03' => 'III',
						'04' => 'IV',
						'05' => 'V',
						'06' => 'VI',
						'07' => 'VII',
						'08' => 'VIII',
						'09' => 'IX',
						'10' => 'X',
						'11' => 'XI',
						'12' => 'XII',	
						);
		return $romawi[$month];
	}

	function button($do, $view, $site_url, $view_title){
		$this->site_url_generator($site_url);	
		$this->class_generator($do, $view);
		$this->output_action = $view;
		$this->output_title = $view_title;
		$this->trigger_icon = $this->do_icon[$do];
		return $this->trigger_generator();

	}

	function button_mini($do, $view, $site_url, $view_title){
		$this->site_url_generator($site_url);	
		$this->class_generator($do, $view, "mini");
		$this->output_action = $view;
		$this->output_title = $view_title;
		$this->trigger_icon = $this->do_icon[$do];
		return $this->trigger_generator();
	}

	function button_large($do, $view, $site_url, $view_title){
		$this->site_url_generator($site_url);	
		$this->class_generator($do, $view, "large");
		$this->output_action = $view;
		$this->output_title = $view_title;
		$this->trigger_icon = $this->do_icon[$do];
		return $this->trigger_generator();
	}

	function link_icon($do, $view, $site_url, $view_title){
		$this->site_url_generator($site_url);	
		$this->class_generator($do, $view);
		$this->output_action = $view;
		$this->output_title  = $view_title;
		$this->trigger_icon  = $this->do_icon[$do];
		$this->trigger_do    = $do;
		return $this->trigger_generator("table");
	}

	function site_url_generator($site_url){
		$site_url   = site_url($site_url);
		$this->href = $site_url;
	}

	function class_generator($do, $view, $size=""){
		$this->class = "btn ";
		$this->class .= $this->button_color[$do]." ";
		$this->class .= $size." ";
		$this->class .= $this->view_output[$view];

	}

	function trigger_generator($place="free"){
		switch ($place) {
			case 'free':
				$elemen = "<a href='".$this->href."' class='".$this->class."' output-action='".$this->output_action."' output-title='".$this->output_title."'>".$this->trigger_icon." ".$this->output_title."</a>";
				break;
			case 'table':
				$elemen = "<a href='".$this->href."' class='tooltips show-".$this->output_action."' output-action='".$this->output_action."' output-title='".$this->output_title."' data-placement='top' data-original-title='".$this->trigger_do."'> ".$this->trigger_icon." </a>";
				break;
			default:
				$elemen = "";
				break;
		}
		
		return $elemen;
	}

	function input_text($label, $nameid, $value="", $class="", $validation=""){

		if($class==""){
			$class = "m-wrap large";
		}

		if (strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}

		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = $this->input_template;
		$elemen = str_replace("{type}", "text", $elemen);
		$elemen = str_replace("{name}", $name, $elemen);
		$elemen = str_replace("{id}", $id, $elemen);
		$elemen = str_replace("{class}", $class, $elemen);
		$elemen = str_replace("{value}", $value, $elemen);
		$this->field_elemen     = $elemen;

        return $this->field_output();
	}

	function input_datepicker($label, $nameid, $value="", $format="dd-mm-yyyy", $class="", $validation=""){

		if($class==""){
			$class = "input-append date date-picker";
		}

		if (strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}
        
		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = $this->datepicker_template;
		$elemen = str_replace("{name}", $name, $elemen);
		$elemen = str_replace("{id}", $id, $elemen);
		$elemen = str_replace("{class}", $class, $elemen);
		$elemen = str_replace("{value}", $value, $elemen);
		$elemen = str_replace("{format}", $format, $elemen);
		$this->field_elemen     = $elemen;

        return $this->field_output();
	}

	function input_file_upload($label, $nameid, $value="", $class="", $validation=""){
		if($class==""){
			$class = "fileupload fileupload-new";
		}

		if (strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}
        
		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = $this->input_file_upload_template;
		$elemen = str_replace("{name}", $name, $elemen);
		$elemen = str_replace("{id}", $id, $elemen);
		$elemen = str_replace("{class}", $class, $elemen);
		
		$this->field_elemen     = $elemen;

        return $this->field_output();
	}

	function image_upload($label, $nameid, $value="", $class="", $validation=""){
		if($class==""){
			$class = "fileupload fileupload-new";
		}

		if (strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}
        
		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = $this->image_upload_template;
		$elemen = str_replace("{name}", $name, $elemen);
		$elemen = str_replace("{id}", $id, $elemen);
		$elemen = str_replace("{class}", $class, $elemen);
		
		$this->field_elemen     = $elemen;

        return $this->field_output();
	}

	function select($label, $nameid, $option, $placeholder, $option_attr="", $value="", $class="", $validation=""){

		if($class == ""){
			$class = "chosen";
		}

		if(strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}

		if(strpos($option_attr, '|') !== false) {  
			$arr          = explode("|", $option_attr);
			$option_value = $arr[0];
			$option_name  = $arr[1];
		}else{
			$option_value = $option_attr;
			$option_name  = $option_attr;
		}

		$this->field_label      = $label;
		$this->field_validation = $validation;
		$select_elemen          = $this->select_template;
		$select_elemen          = str_replace("{name}", $name, $select_elemen);
		$select_elemen          = str_replace("{id}", $id, $select_elemen);
		$select_elemen          = str_replace("{class}", $class, $select_elemen);
		$select_elemen          = str_replace("{placeholder}", $placeholder, $select_elemen);
		$option_elemens         = "";
        if(is_object($option)){
        	foreach($option->result() as $row) {
	            $option_elemen = $this->option_template;
	            
	            if(!empty($value) && $row->$option_value == $value){
	            	$option_elemen = str_replace("{selected}", " selected='selected' ", $option_elemen);
	            }else{
	            	$option_elemen = str_replace("{selected}", "", $option_elemen);
	            }
				$option_elemen  = str_replace("{option_value}", $row->$option_value, $option_elemen);
				$option_elemen  = str_replace("{option_name}", $row->$option_name, $option_elemen);
				$option_elemens .= $option_elemen;
        	}
        }elseif(is_array($option)){
        	foreach($option as $row) {
	            $option_elemen = $this->option_template;
	            
	            if(strpos($row, '|') !== false) {  
					$arr          = explode("|", $row);
					$option_value = $arr[0];
					$option_name  = $arr[1];
	            }else{
	            	$option_value = $option_name = $row;
	            }	

	            if(!empty($value) && $option_value == $value){
	            	$option_elemen = str_replace("{selected}", " selected='selected' ", $option_elemen);
	            }else{
	            	$option_elemen = str_replace("{selected}", "", $option_elemen);
	            }
	            
	            $option_elemen = str_replace("{option_value}", $option_value, $option_elemen);
	            $option_elemen = str_replace("{option_name}", $option_name, $option_elemen);
	            $option_elemens .= $option_elemen;
        	}
        }
                        
      	$this->field_elemen = str_replace("{option_place}", $option_elemens, $select_elemen);

        return $this->field_output();
	}

	function select_group($label, $nameid, $group_attr, $option, $placeholder, $option_attr, $group_label="", $value="", $class="", $validation=""){
		if($class == ""){
			$class = "chosen";
		}

		if(strpos($nameid, '|') !== false) {  
			$arr  = explode("|", $nameid);
			$name = $arr[0];
			$id   = $arr[1];
		}else{
			$name = $nameid;
			$id   = $nameid;
		}

		if(strpos($option_attr, '|') !== false) {  
			$arr          = explode("|", $option_attr);
			$option_value = $arr[0];
			$option_name  = $arr[1];
		}

		if(strpos($group_attr, '|') !== false) {  
			$arr        = explode("|", $group_attr);
			$group_id   = $arr[0];
			$group_name = $arr[1];
		}

		$this->field_label      = $label;
		$this->field_validation = $validation;

		$select_elemen          = $this->select_template;
		$select_elemen          = str_replace("{name}", $name, $select_elemen);
		$select_elemen          = str_replace("{id}", $id, $select_elemen);
		$select_elemen          = str_replace("{class}", $class, $select_elemen);
		$select_elemen          = str_replace("{placeholder}", $placeholder, $select_elemen);
		$option_elemens         = "";

        $grouping = '';          
        foreach($option->result() as $row) {
        	if($grouping != $row->$group_id){
                if($grouping!=''){
                    $option_elemens .= "</optgroup>";
            	}
            	if($group_label == ""){

            	}
            	$grouping = $row->$group_id;
            	$option_elemens .= "<optgroup label='".$group_label." ".$row->$group_name."'>";
            }
            	
            $option_elemen = $this->option_template;
	            
            if(!empty($value) && $row->$option_value == $value){
            	$option_elemen = str_replace("{selected}", " selected='selected' ", $option_elemen);
            }else{
            	$option_elemen = str_replace("{selected}", "", $option_elemen);
            }
			$option_elemen  = str_replace("{option_value}", $row->$option_value, $option_elemen);
			$option_elemen  = str_replace("{option_name}", $row->$option_name, $option_elemen);
			$option_elemens .= $option_elemen;
        }
                        
        $this->field_elemen = str_replace("{option_place}", $option_elemens, $select_elemen);

        return $this->field_output();
	}


	function radio($label, $name, $option, $value="", $class="", $validation=""){

		if($class==""){
			$class = "radio line";
		}

		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = "";

        foreach ($option as $row) {
        	$option_elemen = $this->radio_template;
        	if(strpos($row, '|') !== false) {  
				$arr          = explode("|", $row);
				$option_value = $arr[0];
				$option_name  = $arr[1];
			}else{
				$option_value = $option_name  = $row;
			}
			if(!empty($value) && $option_value == $value){
	            	$option_elemen = str_replace("{selected}", " checked='checked' ", $option_elemen);
	            }else{
	            	$option_elemen = str_replace("{selected}", "", $option_elemen);
	            }
        	$option_elemen  = str_replace("{option_value}", $option_value, $option_elemen);
			$option_elemen  = str_replace("{option_name}", $name, $option_elemen);
			$option_elemen  = str_replace("{radio_label}", $option_name, $option_elemen);
			$option_elemen  = str_replace("{class}", $class, $option_elemen);
			$elemen .= $option_elemen;
        }

        $this->field_elemen = $elemen;

        return $this->field_output();
	}

	function textarea($label, $nameid, $value="", $class="", $validation=""){

		if($class==""){
			$class = "span6 m-wrap";
		}

		if (strpos($nameid, '|') !== false) {  
  			$arr = explode("|", $nameid);
  			$name = $arr[0];
  			$id = $arr[1];
		}else{
			$name = $nameid;
			$id = $nameid;
		}

		$this->field_label      = $label;
		$this->field_validation = $validation;
		$elemen = $this->textarea_template;
		$elemen = str_replace("{name}", $name, $elemen);
		$elemen = str_replace("{id}", $id, $elemen);
		$elemen = str_replace("{class}", $class, $elemen);
		$elemen = str_replace("{value}", $value, $elemen);
		$this->field_elemen = $elemen;

        return $this->field_output();
	}

	function text($label, $value){

		$elemen = str_replace("{text}", $value, $this->field_text);
		$this->field_label  = $label;
		$this->field_elemen = $elemen;

		return $this->field_output();
	}

	function form_text(){
		return $this->form_text;
	}

	function close_form_text(){
		return "</div>";
	}

	function table_open($id="table-standar"){
		$elemen = str_replace("{id}", $id, $this->table_open);
		return $elemen;
	}

	function table_close(){
		return $this->table_close;
	}

	function field_output(){
		$output = $this->field_template;
		$output = str_replace("{label_place}", $this->field_label, $output);
		$output = str_replace("{field_place}", $this->field_elemen, $output);
		$output = str_replace("{field_validation}", $this->field_validation, $output);

		return $output;
	}

	function print_button($pdf="", $excel=""){
		if($pdf != ""){
			$pdf_link = site_url($pdf);
			$pdf = "<li><a href='$pdf_link' target='_blank'><i class='icon-file'></i> Save as PDF</a></li>";
		}
		if($excel != ""){
			$excel_link = site_url($excel);
			$excel = "<li><a href='$excel_link' target='_blank'><i class='icon-table'></i> Export to Excel</a></li>";
		}
		$output = '<div class="btn-group">
				<button class="btn dropdown-toggle blue" data-toggle="dropdown"><i class="icon-print"></i> Print <i class="icon-angle-down"></i>
				</button>
				<ul class="dropdown-menu pull-right">
					{pdf}
					{excel}
				</ul>
			</div>';
		$output = str_replace("{pdf}", $pdf, $output);
		$output = str_replace("{excel}", $excel, $output);	
		return $output;
	}
	
}

/* End of file xm.php */
/* Location: .//C/xampp/htdocs/lite/aplikasi/libraries/xm.php */


?>