<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tahun_ajaran_model extends CI_Model {

	function get($id){
		$this->db->where("id_tahun_ajaran", $id);
		$data = $this->db->get("ak_mtr_tahun_ajaran");
		return $data->row();
	}

	function aktif(){
		$this->db->where("status", "aktif");
		$this->db->order_by("id_tahun_ajaran", "desc");
		$data = $this->db->get("ak_mtr_tahun_ajaran");
		return $data->row();	
	}

	function get_all(){
		$data = $this->db->get("ak_mtr_tahun_ajaran");
		return $data;
	}

	function insert(){
		$tahun_ajaran 	= $this->input->post("tahun_ajaran");

		$this->db->set("tahun_ajaran", $tahun_ajaran);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_mtr_tahun_ajaran");
		return $this->db->insert_id();
	}

	function update(){
		$tahun_ajaran    = $this->input->post("tahun_ajaran");
		$id_tahun_ajaran = $this->input->post("id_tahun_ajaran");

		$this->db->set("tahun_ajaran", $tahun_ajaran);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_tahun_ajaran", $id_tahun_ajaran);
		$this->db->update("ak_mtr_tahun_ajaran");
		
	}

	function delete($id){
		$this->db->where("id_tahun_ajaran", $id_tahun_ajaran);
		$this->db->delete("ak_mtr_tahun_ajaran");
	}

}

/* End of file fakultas_model.php */
/* Location: ./application/models/fakultas_model.php */

?>