<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fakultas_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_fakultas", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_fakultas");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_fakultas");
		return $data;
	}

	function insert(){
		$kode_fakultas = $this->input->post("kode_fakultas");
		$singkatan     = $this->input->post("singkatan");
		$nama_fakultas = $this->input->post("nama_fakultas");
		//$sk            = $this->input->post("sk");
		$dekan         = $this->input->post("dekan");
		$pd1           = $this->input->post("pd1");
		$pd2           = $this->input->post("pd2");
		$pd3           = $this->input->post("pd3");
		$kurikulum     = $this->input->post("kurikulum");
		$genap_ganjil  = $this->input->post("genap_ganjil");
		$wakil_dekan   = $this->input->post("wakil_dekan");

		$this->db->set("kode_fakultas", $kode_fakultas);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("nama_fakultas", $nama_fakultas);
		//$this->db->set("sk", $sk);
		$this->db->set("dekan", $dekan);
		$this->db->set("wakil_dekan", $wakil_dekan);
		$this->db->set("pd1", $pd1);
		$this->db->set("pd2", $pd2);
		$this->db->set("pd3", $pd3);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->set("genap_ganjil", $genap_ganjil);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("fakultas");
		return $this->db->insert_id();
	}

	function update(){
		$id_fakultas   = $this->input->post("id_fakultas");
		$kode_fakultas = $this->input->post("kode_fakultas");
		$singkatan     = $this->input->post("singkatan");
		$nama_fakultas = $this->input->post("nama_fakultas");
		//$sk            = $this->input->post("sk");
		$dekan         = $this->input->post("dekan");
		$pd1           = $this->input->post("pd1");
		$pd2           = $this->input->post("pd2");
		$pd3           = $this->input->post("pd3");
		$kurikulum     = $this->input->post("kurikulum");
		$genap_ganjil  = $this->input->post("genap_ganjil");
		$wakil_dekan   = $this->input->post("wakil_dekan");

		$this->db->set("kode_fakultas", $kode_fakultas);
		$this->db->set("singkatan", $singkatan);
		$this->db->set("nama_fakultas", $nama_fakultas);
		//$this->db->set("sk", $sk);
		$this->db->set("dekan", $dekan);
		$this->db->set("wakil_dekan", $wakil_dekan);
		$this->db->set("pd1", $pd1);
		$this->db->set("pd2", $pd2);
		$this->db->set("pd3", $pd3);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->set("genap_ganjil", $genap_ganjil);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_fakultas", $id_fakultas);
		$this->db->update("fakultas");
		
	}

	function delete($id){
		$this->db->where("id_fakultas", $id);
		$this->db->delete("fakultas");
	}

}

/* End of file fakultas_model.php */
/* Location: ./application/models/fakultas_model.php */

?>