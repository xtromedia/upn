<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maba_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_calon_mahasiswa", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_maba");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_maba");
		return $data;
	}

	function register_status($id)
	{
		$this->db->set("status_akademik", '1');
		$this->db->where("id_calon_mahasiswa", $id);
		$this->db->update("calon_mahasiswa");
	}

}

/* End of file maba_model.php */
/* Location: ./application/models/maba_model.php */

?>