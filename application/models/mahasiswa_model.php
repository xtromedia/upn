<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_mahasiswa", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_mahasiswa");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_mahasiswa");
		return $data;
	}

	function get_all_by_fakultas($id){
		$data = $this->db->where("id_fakultas", $id);
		$data = $this->db->get("vw_mahasiswa");
		return $data;
	}

	function get_all_by_jurusan($id){
		$data = $this->db->where("id_fakultas", $id);
		$data = $this->db->get("vw_jurusan");
		return $data;
	}

	function get_all_by_cuti(){
		$data = $this->db->get("vw_cuti_mahasiswa");
		return $data;
	}

	function get_all_by_dropout(){
		$data = $this->db->get("vw_dropout_mahasiswa");
		return $data;
	}

	function generate_nim($prodi)
	{
		$thn_ini = date('y');	
		$kode_fakultas = $prodi->kode_fakultas{1};	
		$kode_jurusan = $prodi->kode_jurusan{1};	

		$nim = $kode_fakultas;
		$nim .= '.'.$thn_ini;
		$nim .= '.'.$kode_jurusan;
		$nim .= '.'.$this->get_no_urut();

		return $nim;
	}

	function get_no_urut(){
		$this->db->select_sum('no_urut', 'no_urut');
		$data = $this->db->get('mahasiswa')->row();
		$no_urut = $data->no_urut + 1;
		return $no_urut;
	}

	function get_semester($id_mahasiswa=''){
		if($id_mahasiswa == ''){
			$id_mahasiswa = $this->input->post("id_mahasiswa");
		}

		$this->db->select("semester");
		$this->db->where("id_mahasiswa", $id_mahasiswa);
		$data = $this->db->get('vw_mahasiswa')->row();

		return $data->semester;

	}

	function transfer($maba, $prodi){

		$nim 					= $this->generate_nim($prodi);
		$id_calon_mahasiswa 	= $maba->id_calon_mahasiswa;
		$tahun_ajaran 			= $maba->tahun_ajaran;
		$nama 					= $maba->nama;
		$kelamin 				= $maba->kelamin;
		$tempat_lahir 			= $maba->tempat_lahir;
		$tanggal_lahir 			= $maba->tanggal_lahir;
		$kerja 					= $maba->kerja;
		$sekolah_asal 			= $maba->sekolah_asal;
		$jurusan_sekolah 		= $maba->jurusan_sekolah;
		$kabupaten 				= $maba->kabupaten;
		$tahun_tamat 			= $maba->tahun_tamat;
		$alamat 				= $maba->alamat;
		$kodepos 				= $maba->kodepos;
		$telepon 				= $maba->telepon;
		$email 					= $maba->email;
		$agama 					= $maba->agama;
		$warga 					= $maba->warga;
		$status_ortu 			= $maba->status_ortu;
		$nama_ortu 				= $maba->nama_ortu;
		$pekerjaan_ortu 		= $maba->pekerjaan_ortu;
		$alamat_ortu 			= $maba->alamat_ortu;
		$kodepos_ortu 			= $maba->kodepos_ortu;
		$telepon_ortu 			= $maba->telepon_ortu;
		$id_prodi 				= $prodi->id_prodi;
		$no_kuitansi 			= $maba->no_kuitansi;
		$no_ijasah 				= $maba->no_ijasah;
		$tgl_ijasah 			= $maba->tgl_ijasah;
		$status_akademik 		= 'aktif';

		$this->db->set("id_calon_mahasiswa", $id_calon_mahasiswa);
		$this->db->set("tahun_ajaran", $tahun_ajaran);
		$this->db->set("nama", $nama);
		$this->db->set("kelamin", $kelamin);
		$this->db->set("tempat_lahir", $tempat_lahir);
		$this->db->set("tanggal_lahir", $tanggal_lahir);
		$this->db->set("kerja", $kerja);
		$this->db->set("sekolah_asal", $sekolah_asal);
		$this->db->set("jurusan_sekolah", $jurusan_sekolah);
		$this->db->set("kabupaten", $kabupaten);
		$this->db->set("tahun_tamat", $tahun_tamat);
		$this->db->set("alamat", $alamat);
		$this->db->set("kodepos", $kodepos);
		$this->db->set("telepon", $telepon);
		$this->db->set("email", $email);
		$this->db->set("agama", $agama);
		$this->db->set("warga", $warga);
		$this->db->set("status_ortu", $status_ortu);
		$this->db->set("nama_ortu", $nama_ortu);
		$this->db->set("pekerjaan_ortu", $pekerjaan_ortu);
		$this->db->set("alamat_ortu", $alamat_ortu);
		$this->db->set("kodepos_ortu", $kodepos_ortu);
		$this->db->set("telepon_ortu", $telepon_ortu);
		$this->db->set("id_prodi", $id_prodi);
		$this->db->set("no_kuitansi", $no_kuitansi);
		$this->db->set("tgl_daftar_ulang", "now()", false);
		$this->db->set("no_ijasah", $no_ijasah);
		$this->db->set("tgl_ijasah", $tgl_ijasah);
		$this->db->set("no_urut", $this->get_no_urut());
		$this->db->set("nim", $nim);
		$this->db->set("status_akademik", $status_akademik);
		$this->db->set("semester", '1');
		$this->db->set("periode", '0');
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("mahasiswa");
		return $this->db->insert_id();
	}

	function update(){

		$nama 					= $this->input->post("singkatan");
		$kelamin 				= $this->input->post("kelamin");
		$tempat_lahir 			= $this->input->post("tempat_lahir");
		$tanggal_lahir 			= $this->xm->format_tanggal($this->input->post("tanggal_lahir"), "Y-m-d");
		$email 					= $this->input->post("email");
		$alamat 				= $this->input->post("alamat");
		$kodepos 				= $this->input->post("kodepos");
		$telepon 				= $this->input->post("telepon");
		$agama 					= $this->input->post("agama");
		$status_akademik 		= $this->input->post("status_akademik");
		$kerja 					= $this->input->post("kerja");
		$id_prodi 				= $this->input->post("id_prodi");
		$nama_ortu 				= $this->input->post("nama_ortu");
		$pekerjaan_ortu 		= $this->input->post("pekerjaan_ortu");
		$alamat_ortu 			= $this->input->post("alamat_ortu");
		$kodepos_ortu 			= $this->input->post("kodepos_ortu");
		$telepon_ortu 			= $this->input->post("telepon_ortu");
		$warga 					= $this->input->post("warga");
		$sekolah_asal 			= $this->input->post("sekolah_asal");
		$jurusan_sekolah 		= $this->input->post("jurusan_sekolah");
		$kabupaten 				= $this->input->post("kabupaten");
		$no_ijasah 				= $this->input->post("no_ijasah");
		$tgl_ijasah 			= $this->input->post("tgl_ijasah");
		$tahun_ajaran 			= $this->input->post("tahun_ajaran");
		$tahun_tamat 			= $this->input->post("tahun_tamat");
		$id_mahasiswa 			= $this->input->post("id_mahasiswa");

		$this->db->set("tahun_ajaran", $tahun_ajaran);
		$this->db->set("nama", $nama);
		$this->db->set("kelamin", $kelamin);
		$this->db->set("tempat_lahir", $tempat_lahir);
		$this->db->set("tanggal_lahir", $tanggal_lahir);
		$this->db->set("kerja", $kerja);
		$this->db->set("sekolah_asal", $sekolah_asal);
		$this->db->set("jurusan_sekolah", $jurusan_sekolah);
		$this->db->set("kabupaten", $kabupaten);
		$this->db->set("tahun_tamat", $tahun_tamat);
		$this->db->set("alamat", $alamat);
		$this->db->set("kodepos", $kodepos);
		$this->db->set("telepon", $telepon);
		$this->db->set("email", $email);
		$this->db->set("agama", $agama);
		$this->db->set("warga", $warga);
		$this->db->set("nama_ortu", $nama_ortu);
		$this->db->set("pekerjaan_ortu", $pekerjaan_ortu);
		$this->db->set("alamat_ortu", $alamat_ortu);
		$this->db->set("kodepos_ortu", $kodepos_ortu);
		$this->db->set("telepon_ortu", $telepon_ortu);
		$this->db->set("no_ijasah", $no_ijasah);
		$this->db->set("tgl_ijasah", $tgl_ijasah);
		$this->db->set("id_prodi", $id_prodi);
		$this->db->set("status_akademik", $status_akademik);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_mahasiswa", $id_mahasiswa);
		$this->db->update("mahasiswa");		
	}

	function update_status_cuti()
	{
		$id_mahasiswa 			= $this->input->post("id_mahasiswa");
		$semester 				= $this->input->post("semester");
		$keterangan 			= $this->input->post("keterangan");

		$this->db->set("id_mahasiswa", $id_mahasiswa);
		$this->db->set("lama_semester", $semester);
		$this->db->set("id_tahun_ajaran", '1');
		$this->db->set("keterangan", $keterangan);
		$this->db->set("status_cuti", 'Aktif');
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_trx_cuti");
		return $this->db->insert_id();		
	}

	function update_status_dropout()
	{
		$id_mahasiswa 			= $this->input->post("id_mahasiswa");
		$keterangan 			= $this->input->post("keterangan");

		$this->db->set("id_mahasiswa", $id_mahasiswa);
		$this->db->set("id_tahun_ajaran", '1');
		$this->db->set("keterangan", $keterangan);
		$this->db->set("status_dropout", 'Aktif');
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_trx_dropout");
		return $this->db->insert_id();		
	}

	function update_status_akademik($value,$id_mahasiswa)
	{
		$this->db->set("status_akademik", $value);

		$this->db->where("id_mahasiswa", $id_mahasiswa);
		$this->db->update("mahasiswa");		
	}
}

/* End of file mahasiswa_model.php */
/* Location: ./application/models/mahasiswa_model.php */

?>