<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mata_kuliah_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("id_mata_kuliah", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_mata_kuliah");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_mata_kuliah");
		return $data;
	}

	function get_order()
	{
		$this->db->order_by('semester');
		$data = $this->db->get("vw_mata_kuliah");
		return $data;		
	}

	function insert(){
		$id_prodi 			= $this->input->post("id_prodi");
		$id_kategori    	= $this->input->post("id_kategori");
		$kode_mk    		= $this->input->post("kode_mk");
		$ina_mk 		  	= $this->input->post("ina_mk");
		$en_mk        		= $this->input->post("en_mk");
		$kurikulum   		= $this->input->post("kurikulum");
		$sks   				= $this->input->post("sks");
		$semester   		= $this->input->post("semester");
		$syarat_mk   		= $this->input->post("syarat_mk");
		$syarat_sks   		= $this->input->post("syarat_sks");


		$this->db->set("id_prodi", $id_prodi);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->set("semester", $semester);
		$this->db->set("kode", $kode_mk);
		$this->db->set("nama", $ina_mk);
		$this->db->set("sks", $sks);
		$this->db->set("kategori_mk", $id_kategori);
		$this->db->set("konversi", '0');
		$this->db->set("status", '1');
		$this->db->set("syarat_sks", $syarat_sks);
		$this->db->set("syarat_mk", $syarat_mk);
		$this->db->set("nama_en", $en_mk);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_mtr_mata_kuliah");
		return $this->db->insert_id();
	}

	function update(){
		$id_prodi 			= $this->input->post("id_prodi");
		$id_kategori    	= $this->input->post("id_kategori");
		$kode_mk    		= $this->input->post("kode_mk");
		$ina_mk 		  	= $this->input->post("ina_mk");
		$en_mk        		= $this->input->post("en_mk");
		$kurikulum   		= $this->input->post("kurikulum");
		$sks   				= $this->input->post("sks");
		$semester   		= $this->input->post("semester");
		$syarat_mk   		= $this->input->post("syarat_mk");
		$syarat_sks   		= $this->input->post("syarat_sks");
		$id_mk   			= $this->input->post("id_mk");

		$this->db->set("id_prodi", $id_prodi);
		$this->db->set("kurikulum", $kurikulum);
		$this->db->set("semester", $semester);
		$this->db->set("kode", $kode_mk);
		$this->db->set("nama", $ina_mk);
		$this->db->set("sks", $sks);
		$this->db->set("kategori_mk", $id_kategori);
		$this->db->set("konversi", '0');
		$this->db->set("status", '1');
		$this->db->set("syarat_sks", $syarat_sks);
		$this->db->set("syarat_mk", $syarat_mk);
		$this->db->set("nama_en", $en_mk);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_mata_kuliah", $id_mk);
		$this->db->update("ak_mtr_mata_kuliah");		
	}

	function delete($id){
		$this->db->where("id_mata_kuliah", $id);
		$this->db->set("show", 0);
		$this->db->update("ak_mtr_mata_kuliah");
	}

	function search_by_fakultas($id_prodi=""){
		$fakultas = $this->input->post("id_fakultas");

		if($id_prodi != '')
			$this->db->where("id_prodi", $id_prodi);
		$this->db->where("id_fakultas", $fakultas);
		$this->db->where("status", "1");
		$this->db->order_by("semester", "asc");
		$this->db->group_by("nama");
		$data = $this->db->get("vw_mata_kuliah");
		return $data;
	}

	function search_by_semester($id_prodi=""){
		$semester = $this->input->post("semester_mk");
		$fakultas = $this->input->post("id_fakultas");

		$this->db->where("id_fakultas", $fakultas);
		if($semester != '')
			$this->db->where("semester", $semester);
		if($id_prodi != '')
			$this->db->where("id_prodi", $id_prodi);
		$this->db->where("status", "1");
		$this->db->order_by("nama", "asc");
		$this->db->group_by("nama");
		$data = $this->db->get("vw_mata_kuliah");
		return $data;
	}

	function get_in($studi){
		$studi_array = explode(",", $studi);
		$this->db->where_in("id_mata_kuliah" ,$studi_array);
		$data = $this->db->get("vw_mata_kuliah");
		return $data;
	}

}

/* End of file mata_kuliah_model.php */
/* Location: ./application/models/mata_kuliah_model.php */
?>