<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perangkat_dosen_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("kodedosen", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("dosen");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("dosen");
		return $data;
	}

	function insert(){
		$kode_dosen 			= $this->input->post("kode_dosen");
		$kode_fakultas    		= $this->input->post("kode_fakultas");
		$nip    				= $this->input->post("nip");
		$nama_depan 		  	= $this->input->post("nama_depan");
		$nama_belakang        	= $this->input->post("nama_belakang");
		$gelar_depan   			= $this->input->post("gelar_depan");
		$gelar_belakang   		= $this->input->post("gelar_belakang");
		$tgl_lahir   			= $this->input->post("tgl_lahir");
		$tempat_lahir   		= $this->input->post("tempat_lahir");
		$alamat_rumah   		= $this->input->post("alamat_rumah");
		$alamat_kantor   		= $this->input->post("alamat_kantor");
		$telepon_rumah   		= $this->input->post("telepon_rumah");
		$telepon_kantor   		= $this->input->post("telepon_kantor");
		$hp   					= $this->input->post("hp");
		$golongan   			= $this->input->post("golongan");
		$pangkat   				= $this->input->post("pangkat");
		$goldarah   			= $this->input->post("goldarah");
		$kd_agama   			= $this->input->post("kd_agama");
		$e_mail   				= $this->input->post("e_mail");
		$sex   					= $this->input->post("sex");
		$pendidikan   			= $this->input->post("pendidikan");
		$marital_status   		= $this->input->post("marital_status");
		$status_dosen   		= $this->input->post("status_dosen");
		$jabatan   				= $this->input->post("jabatan");
		$dosen_wali   			= $this->input->post("dosen_wali");
		$foto   				= $this->input->post("foto");
		$password   			= md5($this->input->post("password"));
		$tgl_masuk   			= $this->input->post("tgl_masuk");
		$gaji   				= $this->input->post("gaji");
		$satmikal   			= $this->input->post("satmikal");
		$profesi   				= $this->input->post("profesi");
		$jab_fung_id   			= $this->input->post("jab_fung_id");


		$this->db->set("kodedosen", $kode_dosen);
		$this->db->set("kodefak", $kode_fakultas);
		$this->db->set("nip", $nip);
		$this->db->set("nama_bel", $nama_belakang);
		$this->db->set("gelar_depan", $gelar_depan);
		$this->db->set("gelar_bel", $gelar_belakang);
		$this->db->set("tgl_lahir", $tgl_lahir);
		$this->db->set("tempat_lahir", $tempat_lahir);
		$this->db->set("alamat_rumah", $alamat_rumah);
		$this->db->set("alamat_kantor", $alamat_kantor);
		$this->db->set("telp_rumah", $telepon_rumah);
		$this->db->set("telp_kantor", $telepon_kantor);
		$this->db->set("hp", $hp);
		$this->db->set("golongan", $golongan);
		$this->db->set("pangkat", $pangkat);
		$this->db->set("goldarah", $goldarah);
		$this->db->set("kd_agama", $kd_agama);
		$this->db->set("e_mail", $e_mail);
		$this->db->set("sex", $sex);
		$this->db->set("pendidikan", $pendidikan);
		$this->db->set("marital_status", $marital_status);
		$this->db->set("status_dosen", $status_dosen);
		$this->db->set("jabatan", $jabatan);
		$this->db->set("dosenwali", $dosen_wali);
		$this->db->set("foto", $foto);
		$this->db->set("paswd", $password);
		$this->db->set("tgl_masuk", $tgl_masuk);
		$this->db->set("gaji", $gaji);
		$this->db->set("satminkal", $satmikal);
		$this->db->set("profesi", $profesi);
		$this->db->set("jab_fung_id", $jab_fung_id);

		$this->db->insert("dosen");
		return $this->db->insert_id();
	}

	function update(){
		$kode_dosen 			= $this->input->post("kode_dosen");
		$kode_fakultas    		= $this->input->post("kode_fakultas");
		$nip    				= $this->input->post("nip");
		$nama_depan 		  	= $this->input->post("nama_depan");
		$nama_belakang        	= $this->input->post("nama_belakang");
		$gelar_depan   			= $this->input->post("gelar_depan");
		$gelar_belakang   		= $this->input->post("gelar_belakang");
		$tgl_lahir   			= $this->input->post("tgl_lahir");
		$tempat_lahir   		= $this->input->post("tempat_lahir");
		$alamat_rumah   		= $this->input->post("alamat_rumah");
		$alamat_kantor   		= $this->input->post("alamat_kantor");
		$telepon_rumah   		= $this->input->post("telepon_rumah");
		$telepon_kantor   		= $this->input->post("telepon_kantor");
		$hp   					= $this->input->post("hp");
		$golongan   			= $this->input->post("golongan");
		$pangkat   				= $this->input->post("pangkat");
		$goldarah   			= $this->input->post("goldarah");
		$kd_agama   			= $this->input->post("kd_agama");
		$e_mail   				= $this->input->post("e_mail");
		$sex   					= $this->input->post("sex");
		$pendidikan   			= $this->input->post("pendidikan");
		$marital_status   		= $this->input->post("marital_status");
		$status_dosen   		= $this->input->post("status_dosen");
		$jabatan   				= $this->input->post("jabatan");
		$dosen_wali   			= $this->input->post("dosen_wali");
		$foto   				= $this->input->post("foto");
		$password   			= md5($this->input->post("password"));
		$tgl_masuk   			= $this->input->post("tgl_masuk");
		$gaji   				= $this->input->post("gaji");
		$satmikal   			= $this->input->post("satmikal");
		$profesi   				= $this->input->post("profesi");
		$jab_fung_id   			= $this->input->post("jab_fung_id");


		$this->db->set("kodedosen", $kode_dosen);
		$this->db->set("kodefak", $kode_fakultas);
		$this->db->set("nip", $nip);
		$this->db->set("nama_bel", $nama_belakang);
		$this->db->set("gelar_depan", $gelar_depan);
		$this->db->set("gelar_bel", $gelar_belakang);
		$this->db->set("tgl_lahir", $tgl_lahir);
		$this->db->set("tempat_lahir", $tempat_lahir);
		$this->db->set("alamat_rumah", $alamat_rumah);
		$this->db->set("alamat_kantor", $alamat_kantor);
		$this->db->set("telp_rumah", $telepon_rumah);
		$this->db->set("telp_kantor", $telepon_kantor);
		$this->db->set("hp", $hp);
		$this->db->set("golongan", $golongan);
		$this->db->set("pangkat", $pangkat);
		$this->db->set("goldarah", $goldarah);
		$this->db->set("kd_agama", $kd_agama);
		$this->db->set("e_mail", $e_mail);
		$this->db->set("sex", $sex);
		$this->db->set("pendidikan", $pendidikan);
		$this->db->set("marital_status", $marital_status);
		$this->db->set("status_dosen", $status_dosen);
		$this->db->set("jabatan", $jabatan);
		$this->db->set("dosenwali", $dosen_wali);
		$this->db->set("foto", $foto);
		$this->db->set("paswd", $password);
		$this->db->set("tgl_masuk", $tgl_masuk);
		$this->db->set("gaji", $gaji);
		$this->db->set("satminkal", $satmikal);
		$this->db->set("profesi", $profesi);
		$this->db->set("jab_fung_id", $jab_fung_id);


		$this->db->where("kodedosen", $kodedosen);
		$this->db->update("dosen");
		
	}

	function delete($id){
		$this->db->where("kodedosen", $id);
		$this->db->delete("dosen");
	}

}

/* End of file perangkat_dosen_model.php */
/* Location: ./application/models/perangkat_dosen_model.php */

?>