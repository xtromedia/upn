<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_barang", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("inv_mtr_barang");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("inv_mtr_barang");
		return $data;
	}

	function insert(){
		$id_kategori_brg 	= $this->input->post("id_kategori_brg");
		$nama_barang 		= $this->input->post("nama_barang");
		$id_satuan 			= $this->input->post("id_satuan");
		$jenis 				= $this->input->post("jenis");

		$this->db->set("id_kategori_brg", $id_kategori_brg);
		$this->db->set("nama_barang", $nama_barang);
		$this->db->set("id_satuan", $id_satuan);
		$this->db->set("jenis", $jenis);
		
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("inv_mtr_barang");
		return $this->db->insert_id();
	}

	function update(){
		$id_kategori_brg 	= $this->input->post("id_kategori_brg");
		$nama_barang 		= $this->input->post("nama_barang");
		$id_satuan 			= $this->input->post("id_satuan");
		$jenis 				= $this->input->post("jenis");
		$id_barang 			= $this->input->post("id_barang");

		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');
		
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_barang", $id_barang);
		$this->db->update("inv_mtr_barang");
		
	}

	function delete($id){
		$this->db->where("id_barang", $id);
		$this->db->delete("inv_mtr_barang");
	}

}

/* End of file barang_model.php */
/* Location: ./application/models/barang_model.php */

?>