<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Satuan_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_satuan", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("inv_mtr_satuan");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("inv_mtr_satuan");
		return $data;
	}

	function insert(){
		$nama_satuan 	= $this->input->post("nama_satuan");

		$this->db->set("nama_satuan", $nama_satuan);		
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("inv_mtr_satuan");
		return $this->db->insert_id();
	}

	function update(){
		$nama_satuan 		= $this->input->post("nama_satuan");
		$id_satuan 		= $this->input->post("id_satuan");

		$this->db->set("nama_satuan", $nama_satuan);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_satuan", $id_satuan);
		$this->db->update("inv_mtr_satuan");
		
	}

	function delete($id){
		$this->db->where("id_satuan", $id);
		$this->db->delete("inv_mtr_satuan");
	}

}

/* End of file satuan_model.php */
/* Location: ./application/models/satuan_model.php */

?>