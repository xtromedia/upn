<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruang_kelas_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){			

			$this->db->where("id_ruang_kelas", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_ruang_kelas");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_ruang_kelas");
		return $data;
	}

	function insert(){
		$nama_ruang 		= $this->input->post("nama_ruang");
		$kapasitas 		  	= $this->input->post("kapasitas");
		$id_fakultas    	= $this->input->post("id_fakultas");
		$keterangan        	= $this->input->post("keterangan");

		$this->db->set("nama_ruang", $nama_ruang);
		$this->db->set("kapasitas", $kapasitas);
		$this->db->set("fakultas", $id_fakultas);
		$this->db->set("keterangan", $keterangan);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ruang_kelas");
		return $this->db->insert_id();
	}

	function update(){
		$nama_ruang 		= $this->input->post("nama_ruang");
		$kapasitas 		  	= $this->input->post("kapasitas");
		$id_fakultas    	= $this->input->post("id_fakultas");
		$keterangan        	= $this->input->post("keterangan");
		$id_ruang_kelas     = $this->input->post("id_ruang_kelas");

		$this->db->set("nama_ruang", $nama_ruang);
		$this->db->set("kapasitas", $kapasitas);
		$this->db->set("fakultas", $id_fakultas);
		$this->db->set("keterangan", $keterangan);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_ruang_kelas", $id_ruang_kelas);
		$this->db->update("ruang_kelas");
		
	}

	function delete($id){
		$this->db->where("id_ruang_kelas", $id);
		$this->db->delete("ruang_kelas");
	}

}

/* End of file ruang_kelas_model.php */
/* Location: ./application/models/ruang_kelas_model.php */

?>