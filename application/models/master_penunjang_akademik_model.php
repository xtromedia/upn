<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_penunjang_akademik_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_master_penunjang_akademik", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("vw_ak_master_penunjang_akademik");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("vw_ak_master_penunjang_akademik");
		return $data;
	}

	function insert(){
		$nama 			= $this->input->post("nama");
		$laboratorium 	= $this->input->post("laboratorium");

		$this->db->set("nama", $nama);
		$this->db->set("laboratorium", $laboratorium);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("ak_master_penunjang_akademik");
		return $this->db->insert_id();
	}

	function update(){
		$nama 			= $this->input->post("nama");
		$laboratorium 	= $this->input->post("laboratorium");
		$id 			= $this->input->post("id");

		$this->db->set("nama", $nama);
		$this->db->set("laboratorium", $laboratorium);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_master_penunjang_akademik", $id);
		$this->db->update("ak_master_penunjang_akademik");		
	}

	function delete($id){
		$this->db->where("id_master_penunjang_akademik", $id);
		$this->db->delete("ak_master_penunjang_akademik");
	}

}

/* End of file master_penunjang_akademik_model.php */
/* Location: ./application/models/master_penunjang_akademik_model.php */

?>