<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_nilai", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("nilai");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("nilai");
		return $data;
	}

	function insert(){
		$initial 	= $this->input->post("initial");
		$index 		= $this->input->post("index");

		$this->db->set("initial", $initial);
		$this->db->set("index", $index);
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("nilai");
		return $this->db->insert_id();
	}

	function update(){
		$initial 	= $this->input->post("initial");
		$index 		= $this->input->post("index");
		$id_nilai 	= $this->input->post("id_nilai");

		$this->db->set("initial", $initial);
		$this->db->set("index", $index);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_nilai", $id_nilai);
		$this->db->update("nilai");
		
	}

	function delete($id){
		$this->db->where("id_nilai", $id);
		$this->db->delete("nilai");
	}

}

/* End of file nilai_model.php */
/* Location: ./application/models/nilai_model.php */

?>