<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suplier_model extends CI_Model {

	function get($id){
		if(is_numeric($id)){
			$this->db->where("id_suplier", $id);
		}else{
			$this->db->where("slug", $id);
		}
		$data = $this->db->get("inv_mtr_suplier");
		return $data->row();
	}

	function get_all(){
		$data = $this->db->get("inv_mtr_suplier");
		return $data;
	}

	function insert(){
		$nama_suplier 	= $this->input->post("nama_suplier");
		$alamat_suplier = $this->input->post("alamat_suplier");
		$tlp_suplier 	= $this->input->post("tlp_suplier");

		$this->db->set("nama_suplier", $nama_suplier);
		$this->db->set("alamat_suplier", $alamat_suplier);
		$this->db->set("tlp_suplier", $tlp_suplier);
		
		$this->db->set("created", "now()", false);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');
		$this->db->set("created_by", '0');

		$this->db->insert("inv_mtr_suplier");
		return $this->db->insert_id();
	}

	function update(){
		$nama_suplier 		= $this->input->post("nama_suplier");
		$alamat_suplier 	= $this->input->post("alamat_suplier");
		$tlp_suplier 		= $this->input->post("tlp_suplier");
		$id_suplier 		= $this->input->post("id_suplier");

		$this->db->set("nama_suplier", $nama_suplier);
		$this->db->set("alamat_suplier", $alamat_suplier);
		$this->db->set("tlp_suplier", $tlp_suplier);
		$this->db->set("modified", "now()", false);
		$this->db->set("modified_by", '0');

		$this->db->where("id_suplier", $id_suplier);
		$this->db->update("inv_mtr_suplier");
		
	}

	function delete($id){
		$this->db->where("id_suplier", $id);
		$this->db->delete("inv_mtr_suplier");
	}

}

/* End of file suplier_model.php */
/* Location: ./application/models/suplier_model.php */

?>