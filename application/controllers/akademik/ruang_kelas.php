<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruang_kelas extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("fakultas_model", "fakultas");
		$this->load->model("jurusan_model", "jurusan");		
		$this->load->model("ruang_kelas_model", "ruangan");
	}

	public function index()
	{
		$data['page_title']       = "Master Ruang Kelas";
		$data['page_description'] = "Manajemen data ruang kelas pada sistem $this->system";
		$data['list_data']		  = $this->ruangan->get_all();
		$this->template_view('akademik/ruang_kelas/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->ruangan->get_all();
		$this->load->view("akademik/ruang_kelas/table", $data);
	}

	function detail($id){
		$data['data']		  = $this->ruangan->get($id);
		$this->modal_view("akademik/ruang_kelas/detail", $data);
	}

	function insert(){
		$data['fakultas']	 = $this->fakultas->get_all();
		$this->modal_view("akademik/ruang_kelas/insert", $data);
	}

	function do_insert(){
		$this->ruangan->insert();
		$this->table();
	}

	function edit($id){
		$data['fakultas']	 = $this->fakultas->get_all();
		$data['data']		  = $this->ruangan->get($id);
		$this->modal_view("akademik/ruang_kelas/edit", $data);
	}

	function do_edit(){
		$this->ruangan->update();
		$this->table();
	}

	function do_delete($id){
		$this->ruangan->delete($id);
		$this->table();
	}

	function doPDF()
	{

		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->ruangan->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("akademik/ruang_kelas/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Ruang Kelas '.$tgl.'.pdf','I');
		//========================================================
	}

	function doExcel()
	{
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data 		= $this->ruangan->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Undiknas University")
		                 ->setDescription("Data Ruang Kelas Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:D3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Ruang Kelas di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Nama Ruangan');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Kapasitas');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Nama Fakultas');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Keterangan');
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->nama_ruang);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->kapasitas);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->nama_fakultas);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $r->keterangan);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:D'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data ruang kelas undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}	
}