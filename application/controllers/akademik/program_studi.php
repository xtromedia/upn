<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program_studi extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("fakultas_model", "fakultas");
		$this->load->model("jurusan_model", "jurusan");
		$this->load->model("pegawai_model", "pegawai");
		$this->load->model("prodi_model", "prodi");

	}

	public function index()
	{
		$data['page_title']       = "Master program studi";
		$data['page_description'] = "Manajemen data program studi pada sistem $this->system";
		$data['list_data']		  = $this->prodi->get_all();
		$this->template_view('akademik/program_studi/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->prodi->get_all();
		$this->load->view("akademik/program_studi/table", $data);
	}

	function detail($id){
		$data['data']		  = $this->prodi->get($id);
		$this->modal_view("akademik/program_studi/detail", $data);
	}

	function insert(){
		$data['fakultas']	 = $this->fakultas->get_all();
		$data['jurusan']	 = $this->jurusan->get_all();
		$data['pegawai']	 = $this->pegawai->get_all();
		$this->modal_view("akademik/program_studi/insert", $data);
	}

	function do_insert(){
		$this->prodi->insert();
		$this->table();
	}

	function edit($id){
		$data['fakultas']	 = $this->fakultas->get_all();
		$data['jurusan']	 = $this->jurusan->get_all();
		$data['pegawai']	 = $this->pegawai->get_all();
		$data['data']		 = $this->prodi->get($id);
		$this->modal_view("akademik/program_studi/edit", $data);
	}

	function do_edit(){
		$this->prodi->update();
		$this->table();
	}

	function do_delete($id){
		$this->prodi->delete($id);
		$this->table();
	}

	function doPDF()
	{
		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->prodi->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("akademik/program_studi/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Program Studi '.$tgl.'.pdf','I');
		//========================================================
	}

	function doExcel()
	{
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data 		= $this->prodi->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Undiknas University")
		                 ->setDescription("Data Program Studi Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:G3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Program Studi di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Kode Prodi');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Nama Prodi');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Ketua Prodi');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'SK BAN-PT');
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'SK Ijin Operasional');
		$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Jurusan');
		$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Fakultas');
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:G4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->kodeprodi);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->nama_prodi);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->ketua_prodi);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $r->sk);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $r->sk_ijin);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $r->nama_jurusan);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $r->nama_fakultas);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:G'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data program studi undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}