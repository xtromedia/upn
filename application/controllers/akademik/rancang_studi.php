<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rancang_studi extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("studi_model", "studi");
		$this->load->model("mahasiswa_model", "mahasiswa");
		$this->load->model("fakultas_model", "fakultas");
		$this->load->model("mata_kuliah_model", "mk");
		$this->load->model("tahun_ajaran_model", "tahun_ajaran");
	}

	public function pengajuan_krs()
	{
		$data['page_title']       = "Rancangan Studi Mahasiswa";
		$data['page_description'] = "Halaman Untuk merancang perencanaan studi mahasiswa per semester";
		$data['mahasiswa_data']   = $this->mahasiswa->get_all();
		$data['fakultas']         = $this->fakultas->get_all();		
		$data['tahun_ajaran']     = $this->tahun_ajaran->aktif();
		$this->template_view('akademik/rancang_studi/main', $data);
	}

	function do_select_fakultas($id){
		$data = $this->mahasiswa->get_all_by_fakultas($id);
		echo json_encode($data->result_array());
	}

	function do_search(){
		$id_mahasiswa				= $this->input->post("id_mahasiswa");
		$mahasiswa 					= $this->mahasiswa->get($id_mahasiswa);
		$data['mahasiswa']          = $mahasiswa;
		$data['list_mk']            = $this->mk->search_by_fakultas($mahasiswa->id_prodi);
		$data['mahasiswa_mk']       = $this->studi->get_rancang_studi();
		$data['mahasiswa_mk_array'] = array();
		$semester                   = $this->mahasiswa->get_semester();
		$data['max_sks']            = $this->studi->get_max_sks($semester);
		$data['id_mahasiswa']       = $id_mahasiswa;
		foreach ($data['mahasiswa_mk']->result() as $list) {
			$data['mahasiswa_mk_array'][] = $list->id_mata_kuliah;
		}
		$this->load->view("akademik/rancang_studi/mk_portlet", $data);
	}

	function do_select_semester(){
		$id_mahasiswa    = $this->input->post("id_mahasiswa");
		$mahasiswa       = $this->mahasiswa->get($id_mahasiswa);
		$data['list_mk'] = $this->mk->search_by_semester($mahasiswa->id_prodi);
		$this->load->view("akademik/rancang_studi/mk_table", $data);	
	}

	function do_cek_syarat(){
		$syarat_mk    = $this->input->post("syarat_mk");
		$syarat_sks   = $this->input->post("syarat_sks");
		$id_mahasiswa = $this->input->post("id_mahasiswa");
		$return       = 1;
		if($syarat_mk != '' | $syarat_sks !=""){
			if($syarat_mk != ''){
				$return = $this->studi->cek_syarat_mk($syarat_mk, $id_mahasiswa);
			}
			if($syarat_sks != '' && $return != 0){
				$return = $this->studi->cek_syarat_sks($syarat_sks, $id_mahasiswa);
			}
			echo $return;
		}else{
			echo $return;
		}
	}

	function do_create_rancang_studi(){
		$studi        = $this->input->post("studi_mahasiswa");
		$id           = $this->input->post("id_mahasiswa");
		$tahun_ajaran = $this->input->post("tahun_ajaran");
		$mahasiswa    = $this->mahasiswa->get($id);
		$studi_list   = $this->mk->get_in($studi);
		$this->studi->insert($mahasiswa, $studi_list, $tahun_ajaran);
	}
	
}