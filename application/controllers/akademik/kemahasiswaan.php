<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kemahasiswaan extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("maba_model", "maba");
		$this->load->model("mahasiswa_model", "mahasiswa");
		$this->load->model("prodi_model", "prodi");
		$this->load->model("agama_model", "agama");

	}

	public function profil_mahasiswa()
	{
		$data['page_title']       = "Profil Mahasiswa";
		$data['page_description'] = "Manajemen data profil mahasiswa pada sistem $this->system";
		$data['list_data']		  = $this->mahasiswa->get_all();
		$this->template_view('akademik/kemahasiswaan/profil_main', $data);
	}

	function profil_table(){
		$data['list_data']		  = $this->mahasiswa->get_all();
		$this->load->view("akademik/kemahasiswaan/profil_table", $data);
	}

	function profil_detail($id){
		$data['data']		  = $this->mahasiswa->get($id);
		$this->modal_view("akademik/kemahasiswaan/profil_detail", $data);
	}

	function profil_edit($id){
		$data['prodi']	 		= $this->prodi->get_all();
		$data['agama']	 		= $this->agama->get_all();
		$data['data']		  	= $this->mahasiswa->get($id);
		$this->modal_view("akademik/kemahasiswaan/profil_edit", $data);
	}

	function do_profil_edit(){
		$this->mahasiswa->update();
		$this->profil_table();
	}

	function pengajuan_cuti($id)
	{
		$data['data']		  	= $this->mahasiswa->get($id);
		$this->modal_view("akademik/kemahasiswaan/profil_pengajuan_cuti", $data);
	}

	function do_mahasiswa_cuti()
	{
		$this->mahasiswa->update_status_akademik("cuti",$this->input->post("id_mahasiswa"));		
		$this->mahasiswa->update_status_cuti();
		$this->profil_table();
	}

	function mahasiswa_cuti()
	{
		$data['page_title']       = "Cuti Mahasiswa";
		$data['page_description'] = "Manajemen data cuti mahasiswa pada sistem $this->system";
		$data['list_data']		  = $this->mahasiswa->get_all_by_cuti();
		$this->template_view("akademik/kemahasiswaan/profil_main_cuti", $data);
	}

	function pengajuan_dropout($id)
	{
		$data['data']		  	= $this->mahasiswa->get($id);
		$this->modal_view("akademik/kemahasiswaan/profil_pengajuan_dropout", $data);
	}

	function do_mahasiswa_dropout()
	{
		$this->mahasiswa->update_status_akademik("dropout",$this->input->post("id_mahasiswa"));		
		$this->mahasiswa->update_status_dropout();
		$this->profil_table();
	}

	function mahasiswa_dropout()
	{
		$data['page_title']       = "Dropout Mahasiswa";
		$data['page_description'] = "Manajemen data dropout mahasiswa pada sistem $this->system";
		$data['list_data']		  = $this->mahasiswa->get_all_by_dropout();
		$this->template_view("akademik/kemahasiswaan/profil_main_dropout", $data);
	}
}