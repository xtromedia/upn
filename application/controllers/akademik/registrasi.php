<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("maba_model", "maba");
		$this->load->model("mahasiswa_model", "mahasiswa");
		$this->load->model("prodi_model", "prodi");

	}

	public function registrasi_maru()
	{
		$data['page_title']       = "Calon Mahasiswa Baru";
		$data['page_description'] = "Manajemen data calon mahasiswa baru pada sistem $this->system";
		$data['list_data']		  = $this->maba->get_all();
		$this->template_view('akademik/registrasi/maru_main', $data);
	}

	function maru_table(){
		$data['list_data']		  = $this->maba->get_all();
		$this->load->view("akademik/registrasi/maru_table", $data);
	}

	function maru_detail($id){
		$data['data']		  = $this->maba->get($id);
		$this->modal_view("akademik/registrasi/maru_detail", $data);
	}

	function maru_form($id){
		$data['prodi']	 		= $this->prodi->get_all();
		$data['data']		  	= $this->maba->get($id);
		$this->modal_view("akademik/registrasi/maru_form", $data);
	}

	function do_transfer(){
		$id_prodi 		= $this->input->post('id_prodi');
		$id_calon_maba 	= $this->input->post('id_calon_maba');

		$maba  = $this->maba->get($id_calon_maba);
		$prodi = $this->prodi->get($id_prodi);

		$this->mahasiswa->transfer($maba,$prodi);
		$this->maba->register_status($id_calon_maba);
		$this->maru_table();
	}
}