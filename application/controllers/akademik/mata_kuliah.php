<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mata_kuliah extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("prodi_model", "prodi");
		$this->load->model("kategori_mk_model", "kategori");
		$this->load->model("mata_kuliah_model", "mk");
	}

	public function index()
	{
		$data['page_title']       = "Master Mata Kuliah";
		$data['page_description'] = "Manajemen data mata kuliah pada sistem $this->system";
		$data['list_data']		  = $this->mk->get_all();
		$this->template_view('akademik/mata_kuliah/main', $data);
	}

	function table(){
		$data['list_data']	= $this->mk->get_all();
		$this->load->view("akademik/mata_kuliah/table", $data);
	}

	function detail($id){
		$data['data']		 = $this->mk->get($id);
		$this->modal_view("akademik/mata_kuliah/detail", $data);
	}

	function insert(){
		$data['order_mk']	= $this->mk->get_order();
		$data['kategori']	= $this->kategori->get_all();
		$data['prodi']	 	= $this->prodi->get_all();
		$this->modal_view("akademik/mata_kuliah/insert", $data);
	}

	function do_insert(){
		$this->mk->insert();
		$this->table();
	}

	function edit($id){
		$data['order_mk']	= $this->mk->get_order();
		$data['kategori']	= $this->kategori->get_all();
		$data['prodi']	 	= $this->prodi->get_all();
		$data['data']	 	= $this->mk->get($id);
		$this->modal_view("akademik/mata_kuliah/edit", $data);
	}

	function do_edit(){
		$this->mk->update();
		$this->table();
	}

	function do_delete($id){
		$this->mk->delete($id);
		$this->table();
	}

	function doPDF()
	{
		ini_set("memory_limit", "1000M");
		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->mk->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("akademik/mata_kuliah/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Mata Kuliah '.$tgl.'.pdf','I');
		//========================================================
	}	

	function doExcel()
	{
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data 		= $this->mk->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Undiknas University")
		                 ->setDescription("Data Mata Kuliah Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:E3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Mata Kuliah di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Kode Mata Kuliah');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Nama Mata Kuliah');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'SKS');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Semester');
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Program Studi');
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:E4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:E4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->kode);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->nama);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->sks);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $r->semester);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $r->nama_prodi);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:E'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data mata kuliah undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}