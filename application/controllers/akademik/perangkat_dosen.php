<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perangkat_dosen extends XM_Controller {

	var $system = 'akademik';

	public function __construct(){
		parent::__construct();
		$this->load->model("perangkat_dosen_model", "perangkat");

	}

	public function index()
	{
		$data['page_title']       = "Master Perangkat & Dosen";
		$data['page_description'] = "Manajemen data preangkat & dosen pada sistem $this->system";
		$data['list_data']		  = $this->perangkat->get_all();
		$this->template_view('akademik/perangkat_dosen/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->perangkat->get_all();
		$this->load->view("akademik/perangkat_dosen/table", $data);
	}

	function detail($id){
		$data['data']		  = $this->perangkat->get($id);
		$this->load->view("akademik/perangkat_dosen/detail", $data);
	}

	function insert(){
		$data['data']	 = $this->perangkat->get_all();
		$this->load->view("akademik/perangkat_dosen/insert", $data);
	}

	function do_insert(){
		$this->perangkat->insert();
		$this->table();
	}

	function edit($id){
		$data['data']		  = $this->perangkat->get($id);
		$this->load->view("akademik/perangkat_dosen/edit", $data);
	}

	function do_edit(){
		$this->perangkat->update();
		$this->table();
	}

	function do_delete($id){
		$this->perangkat->delete($id);
		$this->table();
	}
}