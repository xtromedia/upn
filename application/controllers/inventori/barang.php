<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends XM_Controller {

	var $system = 'inventori';

	public function __construct(){
		parent::__construct();
		$this->load->model("barang_model", "barang");
	}

	public function index()
	{
		$data['page_title']       = "Master Barang";
		$data['page_description'] = "Manajemen data Barang pada sistem $this->system";
		$data['list_data']		  = $this->barang->get_all();
		$this->template_view('inventori/barang/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->barang->get_all();
		$this->load->view("inventori/barang/table", $data);
	}

	function insert(){
		$this->modal_view("inventori/barang/insert");
	}

	function do_insert(){
		$this->barang->insert();
		$this->table();
	}

	function edit($id){
		$data['data']		  = $this->barang->get($id);
		$this->modal_view("inventori/barang/edit", $data);
	}

	function do_edit(){
		$this->barang->update();
		$this->table();
	}

	function do_delete($id){
		$this->barang->delete($id);
		$this->table();
	}

	function doPDF()
	{
		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->barang->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("inventori/barang/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Barang '.$tgl.'.pdf','I');
		//========================================================
	}

	function doExcel(){
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");		
		$data 		= $this->barang->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Barang Undiknas")
		                 ->setDescription("Data Barang Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:C3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Barang di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Kategori Barang');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Nama Barang');
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Jenis Barang');		
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:D4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->id_kategori_brg);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $r->nama_barang);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->id_satuan);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $r->jenis);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:B'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data barang undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}