<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Satuan extends XM_Controller {

	var $system = 'inventori';

	public function __construct(){
		parent::__construct();
		$this->load->model("satuan_model", "satuan");
	}

	public function index()
	{
		$data['page_title']       = "Master Satuan";
		$data['page_description'] = "Manajemen data Satuan pada sistem $this->system";
		$data['list_data']		  = $this->satuan->get_all();
		$this->template_view('inventori/satuan/main', $data);
	}

	function table(){
		$data['list_data']		  = $this->satuan->get_all();
		$this->load->view("inventori/satuan/table", $data);
	}

	function insert(){
		$this->modal_view("inventori/satuan/insert");
	}

	function do_insert(){
		$this->satuan->insert();
		$this->table();
	}

	function edit($id){
		$data['data']		  = $this->satuan->get($id);
		$this->modal_view("inventori/satuan/edit", $data);
	}

	function do_edit(){
		$this->satuan->update();
		$this->table();
	}

	function do_delete($id){
		$this->satuan->delete($id);
		$this->table();
	}

	function doPDF()
	{
		$tgl 			 	= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");
		$data['list_data'] 	= $this->satuan->get_all();
		
		//passing view ke variable dengan set param ke 3 true
	  	$html = $this->load->view("inventori/satuan/pdf", $data, TRUE);
	  
	  	/* 
	  	export PDF menggunakan library mpdf
	  	library ada di folder aplikasi/libraries/mpdf/
	  	dokumentasi library ini ada di http://mpdf1.com/manual/
	  	*/
		$this->load->library('mpdf/mpdf');
		$this->mpdf=new mPDF('utf8','A4','','',10,10,10,10,10,10);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output('Report Satuan '.$tgl.'.pdf','I');
		//========================================================
	}

	function doExcel(){
		$tgl 		= $this->xm->format_tanggal(date('Y-m-d'), "d M Y");		
		$data 		= $this->satuan->get_all();

		$this->load->helper('html'); //libraries phpExcel ini memerlukan helper html
		/*
		export excel menggunakan library phpExcel
		library ini ada pada file aplikasi/libraries/phpExcel.php
		dokumentasi atau contoh library ini ada di folder aplikasi/libraries/phpExcel/documentation/
		*/
		$this->load->library('PHPExcel/IOFactory');
        $this->load->library('PHPExcel');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setTitle("Satuan Undiknas")
		                 ->setDescription("Data Satuan Undiknas");
		                        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);

		// header
		$objPHPExcel->getActiveSheet()->mergeCells("A1:C3");
		$objPHPExcel->getActiveSheet()->setCellValue('A1', "Data Satuan di Undiknas");
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		//===================================================================

		// column header
		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Nama Satuan');
		//===================================================================

		$objPHPExcel->getActiveSheet()->getStyle('A4:A4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A4:A4')->getFill()->getStartColor()->setARGB('DDDDDDDD');
		$objPHPExcel->getActiveSheet()->getStyle('A1:A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:A4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);


		//======= data row	
		$row = 4;
		foreach ($data->result() as $r) {
			$row++;
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r->nama_satuan);
		}
		//===================================================================================

		$styleArray = array(
	             'allborders' => array(
	                 'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
	             )
	         );
			$objPHPExcel->getActiveSheet()->getStyle('A4:B'.$row)->applyFromArray($styleArray);

		// Save it as an excel 2003 file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="data satuan undiknas '.$tgl.'.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}