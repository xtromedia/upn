<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends XM_Controller {

	var $system = 'marketing';

	public function __construct(){
		parent::__construct();
		$this->load->model("menu_model", "menu");
	}

	public function index()
	{
		$data['page_title'] = "Dashboard $this->system";
		$data['page_active'] = "$this->system";
		$data['page_description'] = "Halaman Utama $this->system";
		$this->template_view('template/dashboard', $data);
	}
}