<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa_baru extends XM_Controller {

	var $system = 'marketing';

	public function __construct(){
		parent::__construct();
	}

	public function pendaftaran_test()
	{
		$data['page_title']       = "Pendaftaran Test";
		$data['page_description'] = "Form Pendaftaran Test Mahasiswa Baru";
		$this->template_view('marketing/mahasiswa-baru/pendaftaran-add', $data);
	}
}

/* End of file pendaftaran_test.php */
/* Location: ./application/controllers/marketing/mahasiswa_baru/pendaftaran_test.php */