<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:50px;">No.</th>
			<th>Nama Barang</th>
            <th>Alamat Barang</th>
            <th>Telepon Barang</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama_barang ?></td>
                <td><?php echo $list->alamat_barang ?></td>
                <td><?php echo $list->tlp_barang ?></td>
				<td><?php echo $this->xm->link_icon("edit", "modal", "inventori/barang/edit/$list->id_barang", "Edit Barang $list->nama_barang") ?></td>
				<td><?php echo $this->xm->link_icon("delete", "modal", "inventori/barang/do_delete/$list->id_barang", "Hapus Barang $list->nama_barang") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>