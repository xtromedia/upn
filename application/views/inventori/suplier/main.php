<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Suplier</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "inventori/suplier/insert", "Tambah Suplier"); ?>
			<?php echo $this->xm->print_button("inventori/suplier/doPDF", "inventori/suplier/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("inventori/suplier/table"); ?>
		</div>
	</div>
</div>