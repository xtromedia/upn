<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:50px;">No.</th>
			<th>Nama Suplier</th>
            <th>Alamat Suplier</th>
            <th>Telepon Suplier</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama_suplier ?></td>
                <td><?php echo $list->alamat_suplier ?></td>
                <td><?php echo $list->tlp_suplier ?></td>
				<td><?php echo $this->xm->link_icon("edit", "modal", "inventori/suplier/edit/$list->id_suplier", "Edit Suplier $list->nama_suplier") ?></td>
				<td><?php echo $this->xm->link_icon("delete", "modal", "inventori/suplier/do_delete/$list->id_suplier", "Hapus Suplier $list->nama_suplier") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>