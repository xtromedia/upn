<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>

	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Nama Suplier</th>
            <th>Alamat Suplier</th>
            <th>Telepon Suplier</th>
			<th>Created</th>
			<th>Modified</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nama_suplier ?></td>
            <td><?php echo $list->alamat_suplier ?></td>
            <td><?php echo $list->tlp_suplier ?></td>
			<td><?php echo $this->xm->format_tanggal($list->created, "d M Y") ?></td>
			<td><?php echo $this->xm->format_tanggal($list->modified, "d M Y") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>