<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Kategori Barang</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "inventori/kategori_brg/insert", "Tambah Kategori Barang"); ?>
            <?php echo $this->xm->print_button("inventori/kategori_brg/doPDF", "inventori/kategori_brg/doExcel"); ?>
		</div>
    </div>
    <div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("inventori/kategori_brg/table"); ?>
		</div>
	</div>
</div>
