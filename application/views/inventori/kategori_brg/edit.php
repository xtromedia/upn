<?php echo form_open("inventori/kategori_brg/do_edit", 'class="form-horizontal ajax-handler"'); ?>
    <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
            <input type="hidden" name="id_kategori_brg" id="id_kategori_brg" class="span6 m-wrap" value="<?php echo $data->id_kategori_brg ?>" />
    </div>
    <div class="row-fluid">
        <?php
			echo $this->xm->input_text("Kode Kategori", "kode", $data->kode);
            echo $this->xm->input_text("Nama Kategori", "nama_kategori", $data->nama_kategori);            
            echo $this->xm->textarea("Keterangan", "keterangan", $data->keterangan);
        ?>
    </div>  
<?php echo form_close(); ?>