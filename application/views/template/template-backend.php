<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Undiknas System Admin Panel</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<!-- <link href="<?php echo base_url() ?>assets/plugin/glyphicons/css/glyphicons.css" rel="stylesheet" type="text/css" />
	 -->
	 <link type="text/css" href="<?php echo base_url() ?>assets/plugin/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugin/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugin/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/template-admin/style-metro.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/template-admin/style.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/template-admin/style-responsive.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/template-admin/themes/default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugin/uniform/css/uniform.default.css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="<?php echo base_url() ?>assets/plugin/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url() ?>assets/plugin/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/select2/select2_metro.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugin/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url() ?>assets/css/xtromedia.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/logo/favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed page-footer-fixed">
	<?php $this->load->view("template/template-loading"); ?>
	<!-- BEGIN HEADER -->
	<?php $this->load->view("template/navigation-top") ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php $this->load->view("template/navigation-side") ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							<?php echo $page_title ?> <small><?php echo $page_description ?></small>
						</h3>
						<!-- <ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="#">UI Features</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#">General</a></li>
						</ul> -->
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid" id="first-content">
					<div class="span12">
						<?php $this->load->view($content) ?>
						<?php $this->load->view("template/template-confirm-modal") ?>
						<?php $this->load->view("template/template-alert-modal") ?>
						<div id="popup-modal" class="modal container hide fade in" data-replace="true" data-always-visible="1" data-rail-visible1="1" data-backdrop="static" data-keyboard="false"></div>
					</div>
				</div>		
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- BEGIN PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<div class="footer-inner">
			2013 &copy; Undiknas development by xtromedia team.
		</div>	
		<div class="footer-tools">
			<img src="<?php echo base_url() ?>assets/logo/logo.png" alt="logo" width="50px" style="margin-top:-25px" />
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?php echo base_url() ?>assets/plugin/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/plugin/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?php echo base_url() ?>assets/plugin/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]--> 
	<script src="<?php echo base_url() ?>assets/plugin/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>  
	<script src="<?php echo base_url() ?>assets/plugin/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/plugin/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/plugin/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo base_url() ?>assets/plugin/breakpoints/breakpoints.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
		<!-- TABLE PLUGINS -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/data-tables/DT_bootstrap.js"></script>
	<!-- END PLUGINS -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap/js/bootstrap-tooltip.js"></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap/js/bootstrap-popover.js"></script>
	<script src="<?php echo base_url() ?>assets/js/template-admin/prettify.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/jquery-validation/dist/jquery.validate.min.js"></script>
   	<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/jquery-validation/dist/additional-methods.min.js"></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="<?php echo base_url() ?>assets/plugin/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script src="<?php echo base_url() ?>assets/js/template-admin/app.js"></script>		
	<script src="<?php echo base_url() ?>assets/js/xtromedia.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			xmPluginInit();
			App.init();
			$("#initiate-loading-page").hide();
		});
	</script>
</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>