<div id="alert-modal" class="modal hide slide" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 id="confirm-label" class="text-alert"><i class="icon-exclamation-sign"></i> Pemberitahuan</h4>
	</div>
	<div class="modal-body">		
	</div>
	<div class="modal-footer">
		<button class="btn red" data-dismiss="modal" aria-hidden="true">ok</button>
	</div>
</div>