<?php 
echo $this->xm->form_text();
    echo $this->xm->text("Kode Fakultas", $data->kode_fakultas);
    echo $this->xm->text("Singkatan Fakultas", $data->singkatan);
    echo $this->xm->text("Nama Fakultas", $data->nama_fakultas);
    echo $this->xm->text("Dekan", $data->nama_dekan);
    echo $this->xm->text("Wakil Dekan", $data->nama_wakil_dekan);
    echo $this->xm->text("Pembantu Dekan 1", $data->nama_pd1);
    echo $this->xm->text("Pembantu Dekan 2", $data->nama_pd2);
    echo $this->xm->text("Pembantu Dekan 3", $data->nama_pd3);
    echo $this->xm->text("Tahun Kurikulum", $data->kurikulum);
    echo $this->xm->text("Genap/Ganjil", $data->genap_ganjil);
    echo $this->xm->text("Terakhir Diupdate", $this->xm->format_tanggal($data->modified, "d M Y"));
echo $this->xm->close_form_text(); 
?>