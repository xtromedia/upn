<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Kode Fakultas</th>
			<th>Singkatan</th>
			<th>Nama Fakultas</th>
			<th>Dekan</th>
			<th>Kurikulum</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kode_fakultas ?></td>
			<td><?php echo $list->singkatan ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
			<td><?php echo $list->nama_dekan ?></td>
			<td><?php echo $list->kurikulum ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>