<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Fakultas</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/fakultas/insert", "Tambah Fakultas"); ?>
			<?php echo $this->xm->print_button("akademik/fakultas/doPDF", "akademik/fakultas/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/fakultas/table"); ?>
		</div>
	</div>
</div>