<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th>No.</th>
			<th>Kode Fakultas</th>
			<th>Singkatan</th>
			<th>Nama Fakultas</th>
			<th>Dekan</th>
			<th>Kurikulum</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->kode_fakultas ?></td>
				<td><?php echo $list->singkatan ?></td>
				<td><?php echo $list->nama_fakultas ?></td>
				<td><?php echo $list->nama_dekan ?></td>
				<td><?php echo $list->kurikulum ?></td>
				<td><?php echo $this->xm->link_icon("detail","modal","akademik/fakultas/detail/$list->id_fakultas", "Detail $list->nama_fakultas") ?></td>
				<td><?php echo $this->xm->link_icon("edit","modal","akademik/fakultas/edit/$list->id_fakultas", "Edit $list->nama_fakultas") ?></td>
				<td><?php echo $this->xm->link_icon("delete","modal","akademik/fakultas/do_delete/$list->id_fakultas", "Fakultas $list->nama_fakultas") ?></td>

		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>