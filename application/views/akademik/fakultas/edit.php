<?php echo form_open("akademik/fakultas/do_edit", 'class="form-horizontal ajax-handler"'); ?>
    <div class="form-button">
        <button type="submit" class="btn blue">Simpan</button>
    </div>
    <div class="row-fluid">
        <div class="span6"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
            <input type="hidden" name="id_fakultas" id="id_fakultas" class="span10 m-wrap" value="<?php echo $data->id_fakultas ?>" />
            <?php 
                echo $this->xm->input_text("Kode Fakultas", "kode_fakultas", $data->kode_fakultas); 
                echo $this->xm->input_text("Singkatan Fakultas", "singkatan", $data->singkatan); 
                echo $this->xm->input_text("Nama Fakultas", "nama_fakultas", $data->nama_fakultas); 
                echo $this->xm->input_text("Tahun Kurikulum", "kurikulum", $data->kurikulum); 
                $option = array("Ya", "Tidak");
                echo $this->xm->select("Genap/Ganjil", "genap_ganjil", $option, "Hapus", "", $data->genap_ganjil);
            ?>
        </div>
        <div class="span6">
            <?php 
                echo $this->xm->select("Nama Dekan", "dekan", $pegawai, "Hapus", "id_pegawai|full_name", $data->dekan);
                echo $this->xm->select("Wakil Dekan", "wakil_dekan", $pegawai, "Hapus", "id_pegawai|full_name", $data->wakil_dekan);
                echo $this->xm->select("Pembantu Dekan I", "pd1", $pegawai, "Hapus", "id_pegawai|full_name", $data->pd1);
                echo $this->xm->select("Pembantu Dekan II", "pd2", $pegawai, "Hapus", "id_pegawai|full_name", $data->pd2);
                echo $this->xm->select("Pembantu Dekan III", "pd3", $pegawai, "Hapus", "id_pegawai|full_name", $data->pd3); 
            ?>
        </div> 
    </div>
<?php echo form_close(); ?>