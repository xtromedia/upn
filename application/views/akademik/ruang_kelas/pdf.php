<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Nama Ruangan</th>
			<th>Kapasitas</th>
			<th>Fakultas</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nama_ruang ?></td>
			<td><?php echo $list->kapasitas ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
			<td><?php echo $list->keterangan ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>