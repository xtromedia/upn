<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Ruang Kelas</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/ruang_kelas/insert", "Tambah Ruang Kelas"); ?>
			<?php echo $this->xm->print_button("akademik/ruang_kelas/doPDF", "akademik/ruang_kelas/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/ruang_kelas/table"); ?>
		</div>
	</div>
</div>
