<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Nilai</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/nilai/insert", "Tambah Nilai"); ?>
			<?php echo $this->xm->print_button("akademik/nilai/doPDF", "akademik/nilai/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/nilai/table"); ?>
		</div>
	</div>
</div>
