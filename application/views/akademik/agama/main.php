<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Agama</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/agama/insert", "Tambah Agama"); ?>
			<?php echo $this->xm->print_button("akademik/agama/doPDF", "akademik/agama/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/agama/table"); ?>
		</div>
	</div>
</div>