  <?php 
    echo $this->xm->form_text();
        echo $this->xm->text("Kuitansi #", $data->no_kuitansi);
        echo $this->xm->text("Nama Calon Mahasiswa Baru", $data->nama);
        echo $this->xm->text("Jenis Kelamin", $data->email);        
        echo $this->xm->text("Alamat / Kode pos", $data->alamat.' / '.$data->kodepos);
        echo $this->xm->text("Agama", $data->nama_agama);        
        echo $this->xm->text("Kewarganegaraan", $data->warga);        
        echo $this->xm->text("Pekerjaan", $data->kerja);        
        echo $this->xm->text("Asal / Jurusan / Tahun Tamat Sekolah", $data->sekolah_asal.' - '.$data->kabupaten.' / '.$data->jurusan_sekolah.' / '.$data->tahun_tamat);        
        echo $this->xm->text("No / Tanggal Ijasah", $data->no_ijasah.' / '.$this->xm->format_tanggal($data->tgl_ijasah, "d M Y"));        
        echo $this->xm->text("Fakultas / Jurusan / Prodi Pilihan 1", $data->nama_fakultas1.' / '.$data->nama_jurusan1.' / '.$data->nama_prodi1);        
        echo $this->xm->text("Fakultas / Jurusan / Prodi Pilihan 2", $data->nama_fakultas2.' / '.$data->nama_jurusan2.' / '.$data->nama_prodi2);        
        echo $this->xm->text("Nama Orang Tua", $data->nama_ortu);        
        echo $this->xm->text("Pekerjaan Orang Tua", $data->pekerjaan_ortu);        
        echo $this->xm->text("Alamat / Kode Pos Orang Tua", $data->alamat_ortu.' / '.$data->kodepos_ortu);        
        echo $this->xm->text("Telepon Orang Tua", $data->telepon_ortu);        
        echo $this->xm->text("Asal Info", $data->asal_info);        
        echo $this->xm->text("Sumber", $data->sumber);        
        echo $this->xm->text("Tanggal Daftar", $this->xm->format_tanggal($data->tgl_daftar, "d M Y"));        
        echo $this->xm->text("Gelombang", $data->gelombang);        
        echo $this->xm->text("Terakhir Diupdate", $this->xm->format_tanggal($data->modified, "d M Y"));        
    echo $this->xm->close_form_text(); 
    ?>