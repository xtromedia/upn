<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Prodi</th>
			<th>SK BAN-PT</th>
			<th>SK Ijin Operasional</th>
			<th>Jurusan</th>
			<th>Fakultas</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nama_prodi ?></td>
			<td><?php echo $list->sk ?></td>
			<td><?php echo $list->sk_ijin ?></td>
			<td><?php echo $list->nama_jurusan ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>