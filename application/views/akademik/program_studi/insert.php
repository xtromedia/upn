	<?php echo form_open("akademik/program_studi/do_insert", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
        </div>
        <div class="row-fluid">
            <?php
                echo $this->xm->select_group("Jurusan", "id_jurusan", "id_fakultas|nama_fakultas", $jurusan, "Pilih Jurusan", "id_jurusan|nama_jurusan");
                echo $this->xm->select("Ketua Program Studi", "ketua_prodi",  $pegawai, "Pilih Ketua Program Studi", "id_pegawai|full_name");
                echo $this->xm->input_text("Kode Program Studi", "kode_prodi");
                echo $this->xm->input_text("Nama Program Studi", "nama_prodi");
                echo $this->xm->input_text("SK BAN-PT", "sk");
                echo $this->xm->input_text("SK Ijin Operasional", "sk_ijin");
            ?>
        </div>    
    <?php echo form_close(); ?>