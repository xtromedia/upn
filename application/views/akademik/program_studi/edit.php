<?php echo form_open("akademik/program_studi/do_edit", 'class="form-horizontal ajax-handler"'); ?>
    <div class="form-button">
        <button type="submit" class="btn blue">Simpan</button>
        <input type="hidden" name="id_prodi" id="id_prodi" value="<?php echo $data->id_prodi ?>" />
    </div>
    <div class="row-fluid">
        <?php
            echo $this->xm->select_group("Jurusan", "id_jurusan", "id_fakultas|nama_fakultas", $jurusan, "Hapus", "id_jurusan|nama_jurusan", "", $data->id_jurusan);
            echo $this->xm->select("Ketua Program Studi", "ketua_prodi",  $pegawai, "Hapus", "id_pegawai|full_name", $data->ketua_prodi);
            echo $this->xm->input_text("Kode Program Studi", "kode_prodi", $data->kode_prodi);
            echo $this->xm->input_text("Nama Program Studi", "nama_prodi", $data->nama_prodi);
            echo $this->xm->input_text("SK BAN-PT", "sk", $data->sk);
            echo $this->xm->input_text("SK Ijin Operasional", "sk_ijin", $data->sk_ijin);
        ?>
    </div>    
<?php echo form_close(); ?>