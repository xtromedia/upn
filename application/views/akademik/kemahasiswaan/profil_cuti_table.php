<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:40px;">No.</th>
			<th>NIM</th>
			<th>Nama Mahasiswa</th>
			<th>Cuti Semester</th>
			<th>Tahun Ajaran</th>
			<th>Keterangan</th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nim ?></td>
			<td><?php echo $list->nama ?></td>
			<td><?php echo $list->lama_semester.' Semester' ?></td>
			<td><?php echo $list->tahun_ajaran ?></td>
			<td><?php echo $list->keterangan ?></td>
			<td><?php echo $this->xm->link_icon("detail","modal","akademik/kemahasiswaan/profil_detail/$list->id_mahasiswa", "Detail $list->nama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>