	<?php echo form_open("akademik/kemahasiswaan/do_mahasiswa_cuti", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
            <input type="hidden" name="id_mahasiswa" id="id_mahasiswa" value="<?php echo $data->id_mahasiswa ?>" />
        </div>
        
        <div class="scroll-container">        
            <?php 
                $option_semester = array("1", "2", "3", "4");
                echo $this->xm->select("Lama Cuti Semester", "semester", $option_semester ,"Pilih Semester");
                echo $this->xm->textarea("Keterangan", "keterangan");
            ?>
        </div>
    <?php echo form_close() ?>