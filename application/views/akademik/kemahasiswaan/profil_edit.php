<?php echo form_open("akademik/kemahasiswaan/do_profil_edit", 'class="form-horizontal ajax-handler"'); ?>
    <div class="form-button">
        <button type="submit" class="btn blue">Simpan</button>
        <input type="hidden" name="id_mahasiswa" id="id_mahasiswa" value="<?php echo $data->id_mahasiswa ?>" />
    </div>
    <h3 class="form-section">Informasi Personal</h3>
    <div class="row-fluid">
        <div class="span6"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
            <?php
                echo $this->xm->input_text("Nama", "singkatan", $data->nama); 
                $option = array("Laki-laki", "Perempuan");
                echo $this->xm->select("Jenis Kelamin", "kelamin", $option, "Hapus", "", $data->kelamin);
                echo $this->xm->input_text("Tempat Lahir", "tempat_lahir", $data->tempat_lahir); 
                echo $this->xm->input_datepicker("Tanggal Lahir", "tanggal_lahir", $this->xm->format_tanggal($data->tanggal_lahir, "d M Y"));
                echo $this->xm->input_text("Email", "email", $data->email);
                $option = array("WNI", "WNA");
                echo $this->xm->select("Kewarganegaraan", "warga", $option, "Hapus", "", $data->warga);
                $option = array("Bekerja", "Belum Bekerja");
                echo $this->xm->select("Pekerjaan", "kerja", $option, "Hapus", "", $data->kerja);
                echo $this->xm->select("Agama", "agama", $agama, "Hapus", "id_agama|nama_agama", $data->agama);
                $option = array("aktif", "cuti", "dropout");
            ?>    
        </div>
    </div>    
    <h3 class="form-section">Informasi Alamat</h3>
    <div class="row-fluid">
        <div class="span12"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
            <?php
                echo $this->xm->textarea("Alamat", "alamat", $data->alamat);
                echo $this->xm->input_text("Kode Pos", "kodepos", $data->kodepos);
                echo $this->xm->input_text("Telepon", "telepon", $data->telepon);
            ?>    
        </div>
    </div>    
    <h3 class="form-section">Informasi Akademik</h3>    
    <div class="row-fluid">
        <div class="span12"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->
            <?php 
                echo $this->xm->input_text("Tahun Ajaran", "tahun_ajaran", $data->tahun_ajaran); 
                echo $this->xm->select("Status Akademik", "status_akademik", $option, "Hapus", "", $data->status_akademik);
                echo $this->xm->select_group("Program Studi", "id_prodi", "id_jurusan|nama_jurusan", $prodi, "Hapus", "id_prodi|nama_prodi", "", $data->id_prodi);
            ?>
        </div>
    </div>    
    <h3 class="form-section">Informasi Asal Sekolah</h3>    
    <div class="row-fluid">    
        <div class="span12">
            <?php
                echo $this->xm->input_text("Asal Sekolah", "sekolah_asal", $data->sekolah_asal);
                echo $this->xm->input_text("Jurusan Sekolah", "jurusan_sekolah", $data->jurusan_sekolah);
                echo $this->xm->input_text("Kabupaten Sekolah", "kabupaten", $data->kabupaten);
                echo $this->xm->input_text("No Ijazah", "no_ijasah", $data->no_ijasah);
                echo $this->xm->input_datepicker("Tanggal Ijazah", "tgl_ijasah", $this->xm->format_tanggal($data->tgl_ijasah, "d M Y"));
                echo $this->xm->input_text("Tahun Tamat", "tahun_tamat", $data->tahun_tamat);
            ?>
        </div>
    </div>
    <h3 class="form-section">Informasi Orangtua</h3>    
    <div class="row-fluid">    
        <div class="span12">
            <?php
                echo $this->xm->input_text("Nama Orang Tua", "nama_ortu", $data->nama_ortu);
                echo $this->xm->input_text("Pekerjaan Orang Tua", "pekerjaan_ortu", $data->pekerjaan_ortu);
                echo $this->xm->input_text("Alamat Orang Tua", "alamat_ortu", $data->alamat_ortu);
                echo $this->xm->input_text("kode pos Orang Tua", "kodepos_ortu", $data->kodepos_ortu);
                echo $this->xm->input_text("Telepon Orang Tua", "telepon_ortu", $data->telepon_ortu);
            ?>
        </div>
    </div>            
<?php echo form_close() ?>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $( ".date-picker" ).datepicker();
  });
</script>