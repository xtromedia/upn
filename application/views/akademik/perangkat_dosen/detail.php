<div class="span12 fluid">
	<div class="row-fluid">
		<div class="span4">Kode Dosen</div>
		<div class="span8">: <?php echo $data->kodedosen ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Kode Fakuktas</div>
		<div class="span8">: <?php echo $data->kodefak ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">NIP</div>
		<div class="span8">: <?php echo $data->nip ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Nama Dosen</div>
		<div class="span8">: <?php echo $data->gelar_depan.' '.$data->nama_bel.''.$data->gelar_bel ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Tempat / Tanggal Lahir</div>
		<div class="span8">: <?php echo $data->tempat_lahir.' / '.$this->xtromedia->format_tanggal($data->tgl_lahir, "d M Y") ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Alamat Rumah</div>
		<div class="span8">: <?php echo $data->alamat_rumah ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Alamat Kantor</div>
		<div class="span8">: <?php echo $data->alamat_kantor ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Telepon Rumah</div>
		<div class="span8">: <?php echo $data->telp_rumah ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Telepon Kantor</div>
		<div class="span8">: <?php echo $data->telp_kantor ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Mobile</div>
		<div class="span8">: <?php echo $data->hp ?></div>
	</div>	
	<div class="row-fluid">
		<div class="span4">Pangkat</div>
		<div class="span8">: <?php echo $data->pangkat ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Golongan Darah</div>
		<div class="span8">: <?php echo $data->goldarah ?></div>
	</div>		
	<div class="row-fluid">
		<div class="span4">Agama</div>
		<div class="span8">: <?php echo $data->kd_agama ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Email</div>
		<div class="span8">: <?php echo $data->e_mail ?></div>
	</div>	
	<div class="row-fluid">
		<div class="span4">Jenis Kelamin</div>
		<div class="span8">: <?php if($data->status == 'P'){ echo 'Perempuan'; }elseif($data->status == 'L'){ echo 'Laki-laki'; } ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Pendidikan</div>
		<div class="span8">: <?php echo $data->pendidikan ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Marital Status</div>
		<div class="span8">: <?php echo $data->marital_status ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Status Dosen</div>
		<div class="span8">: <?php if($data->status == '1'){ echo 'Aktif'; }elseif($data->status == '0'){ echo 'Tidak Aktif'; }else{ echo 'Galau'; } ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Jabatan</div>
		<div class="span8">: <?php echo $data->jabatan ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Dosen Wali</div>
		<div class="span8">: <?php echo $data->dosenwali ?></div>
	</div>
</div>