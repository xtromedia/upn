<table class="table table-striped table-bordered table-hover data-tables-handler" id="table-prodi">
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Kode Dosen</th>
			<th>NIP</th>
			<th>Nama Dosen</th>
			<th>Tempat/Tanggal Lahir</th>
			<th>telepon/handphone</th>
			<th>Alamat Rumah</th>
			<th>Alamat Kantor</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kodedosen ?></td>
			<td><?php echo $list->nip ?></td>
			<td><?php echo $list->gelar_depan.' '.$list->nama_depan.' '.$list->nama_bel.' '.$list->gelar_bel ?></td>
			<td><?php echo $list->tempat_lahir.' / '.$this->xtromedia->format_tanggal($list->tgl_lahir, "d M Y") ?></td>
			<td><?php echo $list->telp_rumah.' / '.$list->hp ?></td>			
			<td><?php echo $list->alamat_rumah ?></td>
			<td><?php echo $list->alamat_kantor ?></td>
			<td>
				<?php echo $this->xtromedia->table_detail_modal("akademik/perangkat_dosen/detail/$list->kodedosen", "Detail $list->kodedosen") ?>
			</td>
			<td>
				<?php echo $this->xtromedia->table_edit_modal("akademik/perangkat_dosen/edit/$list->kodedosen", "Edit $list->kodedosen") ?>
			</td>
			<td>
				<?php echo $this->xtromedia->table_detail_modal("akademik/perangkat_dosen/do_delete/$list->kodedosen", "$list->kodedosen") ?>
		</tr>
			<?php
		}
		?>
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function() {
		function disableScreen(){
			$("#disable-screen").show();
		}

		function enableScreen(){
			$("#disable-screen").hide();
		}
		$(".show-modal").click(function(){
			event.preventDefault();
			//disabledScreen();
			var url = $(this).attr("href");
			var elementTitle = $(this).attr("special");
			var title = $(this).attr("modal-title");
			if(elementTitle == 'delete'){
				var answer = confirm("Anda yakin ingin menghapus "+title+" ?");
				if(answer){
					$.ajax({
						url:url,
						type:"POST",
						data:{id:1},
						success:function(result){
							$(".table-container").html(result);
						},
						error:function(){
							alert("something error! contact your developer...");
						}
					});
				}	
			}else{
				$.ajax({
					url:url,
					type:"POST",
					data:{id:1},
					success:function(result){
						$(".modal .modal-header h3").html(title);
						$(".modal .modal-body").html(result);
						$('.modal').modal("show");
					},
					error:function(){
						alert("something error! contact your developer...");
					}
				});
			}
		});
	});
</script>