<div class="span12 fluid">
	<?php echo form_open("akademik/perangkat_dosen/do_edit", 'class="form-horizontal ajax-handler"'); ?>
       <div class="control-group">
          <label class="control-label">Kode Dosen</label>
          <div class="controls input-icon">
             <input type="text" name="kode_dosen" id="kode_dosen" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Kode Fakultas</label>
          <div class="controls input-icon">
             <input type="text" name="kode_fakultas" id="kode_fakultas" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">NIP</label>
          <div class="controls input-icon">
             <input type="text" name="nip" id="nip" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Nama Depan</label>
          <div class="controls input-icon">
             <input type="text" name="nama_depan" id="nama_depan" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Nama Belakang</label>
          <div class="controls input-icon">
             <input type="text" name="nama_belakang" id="nama_belakang" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Gelar Depan</label>
          <div class="controls input-icon">
             <input type="text" name="gelar_depan" id="gelar_depan" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Gelar Belakang</label>
          <div class="controls input-icon">
             <input type="text" name="gelar_belakang" id="gelar_belakang" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Tanggal Lahir</label>
          <div class="controls input-icon">
             <input type="text" name="tgl_lahir" id="tgl_lahir" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Tempat Lahir</label>
          <div class="controls input-icon">
             <input type="text" name="tempat_lahir" id="tempat_lahir" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Alamat Rumah</label>
          <div class="controls input-icon">
             <input type="text" name="alamat_rumah" id="alamat_rumah" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Alamat Kantor</label>
          <div class="controls input-icon">
             <input type="text" name="alamat_kantor" id="alamat_kantor" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Telepon Rumah</label>
          <div class="controls input-icon">
             <input type="text" name="telepon_rumah" id="telepon_rumah" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Telepon Kantor</label>
          <div class="controls input-icon">
             <input type="text" name="telepon_kantor" id="telepon_kantor" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Mobile</label>
          <div class="controls input-icon">
             <input type="text" name="hp" id="hp" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Golongan (M)</label>
          <div class="controls input-icon">
             <input type="text" name="golongan" id="golongan" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Pangkat (M)</label>
          <div class="controls input-icon">
             <input type="text" name="pangkat" id="pangkat" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Golongan Darah (M)</label>
          <div class="controls input-icon">
             <input type="text" name="goldarah" id="goldarah" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Agama (M)</label>
          <div class="controls input-icon">
             <input type="text" name="kd_agama" id="kd_agama" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Email (M)</label>
          <div class="controls input-icon">
             <input type="text" name="e_mail" id="e_mail" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Jenis Kelamin (M)</label>
          <div class="controls input-icon">
             <select class="span6 m-wrap" data-placeholder="Pilih Status" tabindex="1" name="sex" id="sex">
                <option value="">Pilih Status</option>
                <option value="P">Perempuan</option>
                <option value="L">Laki-Laki</option>
             </select>
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Pendidikan (M)</label>
          <div class="controls input-icon">
             <input type="text" name="pendidikan" id="pendidikan" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Marital Status (TT)</label>
          <div class="controls input-icon">
             <input type="text" name="marital_status" id="marital_status" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Status Dosen</label>
          <div class="controls input-icon">
             <select class="span6 m-wrap" data-placeholder="Pilih Status" tabindex="1" name="status_dosen" id="status_dosen">
                <option value="">Pilih Status</option>
                <option value="0">Tidak Aktif</option>
                <option value="1">Aktif</option>
             </select>
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Jabatan (TT)</label>
          <div class="controls input-icon">
             <input type="text" name="jabatan" id="jabatan" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Dosen Wali (TT)</label>
          <div class="controls input-icon">
             <input type="text" name="dosen_wali" id="dosen_wali" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Foto</label>
          <div class="controls input-icon">
             <input type="text" name="foto" id="foto" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Password</label>
          <div class="controls input-icon">
             <input type="password" name="password" id="password" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Tanggal Masuk</label>
          <div class="controls input-icon">
             <input type="text" name="tgl_masuk" id="tgl_masuk" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Gaji</label>
          <div class="controls input-icon">
             <input type="text" name="gaji" id="gaji" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Satmikal (TT)</label>
          <div class="controls input-icon">
             <input type="text" name="satmikal" id="satmikal" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Profesi (M)</label>
          <div class="controls input-icon">
             <input type="text" name="profesi" id="profesi" class="span10 m-wrap" />
          </div>
       </div>
       <div class="control-group">
          <label class="control-label">Jabatan Fungional (M)</label>
          <div class="controls input-icon">
             <input type="text" name="jab_fung_id" id="jab_fung_id" class="span10 m-wrap" />
          </div>
       </div>

       <div class="form-actions">
          <button type="submit" class="btn blue">Simpan</button>
          <button type="button" data-dismiss="modal" class="btn">batal</button>
       </div>
    </form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("form.ajax-handler").submit(function(){
			event.preventDefault();
			var form = $(this);
	      	var arr = form.serializeArray();
	      	$.ajax({
	        	url:form.attr("action"),
		        type:"post",
		        data:arr,
		        success:function(result){
		          $(".table-container").html(result);
		          $(".modal").modal("hide");
		        },
		        error:function(res){
		          alert("error");
		        }
	      	});
		});
	});
</script>