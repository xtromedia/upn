<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Kategori Mata Kuliah</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/kategori_mk/insert", "Tambah Kategori Mata Kuliah"); ?>
            <?php echo $this->xm->print_button("akademik/kategori_mk/doPDF", "akademik/kategori_mk/doExcel"); ?>
		</div>
    </div>
    <div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/kategori_mk/table"); ?>
		</div>
	</div>
</div>
