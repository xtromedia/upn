<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th>No.</th>
			<th>Kode Kategori</th>
			<th>Nama Kategori</th>
			<th>Keterangan</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->singkatan ?></td>
			<td><?php echo $list->nama_kategori ?></td>
			<td><?php echo $list->keterangan ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/kategori_mk/edit/$list->id_kategori_mk", "Edit $list->nama_kategori") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/kategori_mk/do_delete/$list->id_kategori_mk", "$list->nama_kategori") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>
