<div class="portlet box blue">
	<div class="portlet-title">
	  <div class="caption"><i class="icon-reorder"></i>Data Mata Kuliah</div>
	   <div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/mata_kuliah/insert", "Tambah Mata Kuliah"); ?>
            <?php echo $this->xm->print_button("akademik/mata_kuliah/doPDF", "akademik/mata_kuliah/doExcel"); ?>
		</div>
    </div>
    <div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/mata_kuliah/table"); ?>
		</div>
	</div>
</div>
