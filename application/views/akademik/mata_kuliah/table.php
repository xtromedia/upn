<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th style="width:30px;">No.</th>
			<th>Kode MK</th>
			<th>Nama MK</th>
			<th>SKS</th>
			<th>Semester</th>
			<th>Program Studi</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kode ?></td>
			<td><?php echo $list->nama ?></td>
			<td><?php echo $list->sks ?></td>
			<td><?php echo $list->semester ?></td>
			<td><?php echo $list->nama_prodi ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal", "akademik/mata_kuliah/detail/$list->id_mata_kuliah", "Detail $list->nama") ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "akademik/mata_kuliah/edit/$list->id_mata_kuliah", "Edit $list->nama") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "akademik/mata_kuliah/do_delete/$list->id_mata_kuliah", "$list->nama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>