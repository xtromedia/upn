    <?php 
    echo $this->xm->form_text();
        echo $this->xm->text("Program Studi", $data->nama_prodi);
        echo $this->xm->text("Jurusan", $data->nama_jurusan);
        echo $this->xm->text("Kategori MK", $data->singkatan_kategori);
        echo $this->xm->text("Kode MK", $data->kode);
        echo $this->xm->text("MK Indonesia", $data->nama);
        echo $this->xm->text("MK English", $data->nama_en);
        echo $this->xm->text("SKS", $data->sks);
        echo $this->xm->text("Semester", $data->semester);
        echo $this->xm->text("Syarat SKS", $data->syarat_sks);
        echo $this->xm->text("Syarat MK", $data->syarat_mk);
        echo $this->xm->text("Terakhir Diupdate", $this->xm->format_tanggal($data->modified, "d M Y"));    
    echo $this->xm->close_form_text(); 
    ?>