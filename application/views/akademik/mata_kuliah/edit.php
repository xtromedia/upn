	<?php echo form_open("akademik/mata_kuliah/do_edit", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
            <input type="hidden" name="id_mk" id="id_mk" value="<?php echo $data->id_mata_kuliah ?>" />
        </div>
        <div class="row-fluid">
            <div class="span6"> <!-- untuk membuat kolom span6 = 12/jumlah kolom -->     
                <?php
                    echo $this->xm->input_text("Kode MK", "kode_mk", $data->kode);
                    echo $this->xm->input_text("Nama MK Indonesia", "ina_mk", $data->nama);
                    echo $this->xm->input_text("Nama MK English", "en_mk", $data->nama_en);
                    echo $this->xm->input_text("Kurikulum", "kurikulum", $data->kurikulum);
                    echo $this->xm->select_group("Program Studi", "id_prodi", "id_jurusan|nama_jurusan", $prodi, "Hapus", "id_prodi|nama_prodi", "", $data->id_prodi);
                ?>
            </div>
            <div class="span6">
                <?php
                    echo $this->xm->select("Kategori MK", "id_kategori",  $kategori, "Hapus", "id_kategori_mk|singkatan", $data->kategori_mk);
                    $option_sks = array("1|1 SKS", "2|2 SKS", "3|3 SKS", "4|4 SKS", "5|5 SKS", "6|6 SKS");
                    echo $this->xm->select("Jumlah SKS", "sks", $option_sks ,"Hapus", "", $data->sks);
                    $option_semester = array("1", "2", "3", "4", "5", "6", "7", "8");
                    echo $this->xm->select("Semester", "semester", $option_semester ,"Hapus", "", $data->semester);
                    echo $this->xm->select_group("Syarat Mata Kuliah", "syarat_mk", "semester|semester", $order_mk, "Hapus", "id_mata_kuliah|nama", "Semester", $data->syarat_mk);
                    echo $this->xm->input_text("Syarat SKS", "syarat_sks", $data->syarat_sks);

                ?>                
            </div>
        </div>
    <?php echo form_close(); ?>