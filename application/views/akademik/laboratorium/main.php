<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Laboratorium</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/laboratorium/insert", "Tambah Laboratorium"); ?>
			<?php echo $this->xm->print_button("akademik/laboratorium/doPDF", "akademik/laboratorium/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/laboratorium/table"); ?>
		</div>
	</div>
</div>