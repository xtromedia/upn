<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:10px;">No.</th>
			<th>Nama Laboratorium</th>
			<th>Ruangan</th>
			<th>Ketua Laboratorium</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->nama_laboratorium ?></td>
			<td><?php echo $list->nama_ruang ?></td>
			<td><?php echo $list->full_name ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>