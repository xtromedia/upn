<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th>No.</th>
			<th>Nama Laboratorium</th>
			<th>Ruangan</th>
			<th>Ketua Laboratorium</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama_laboratorium ?></td>
				<td><?php echo $list->nama_ruang ?></td>
				<td><?php echo $list->full_name ?></td>
				<td><?php echo $this->xm->link_icon("edit","modal","akademik/laboratorium/edit/$list->id_laboratorium", "Edit $list->nama_laboratorium") ?></td>
				<td><?php echo $this->xm->link_icon("delete","modal","akademik/laboratorium/do_delete/$list->id_laboratorium", "Delete $list->nama_laboratorium") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>