	<?php echo form_open("akademik/jurusan/do_edit", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
            <input type="hidden" name="id_jurusan" id="id_jurusan" class="span10 m-wrap" value="<?php echo $data->id_jurusan ?>" />
        </div>
        <div class="row-fluid">
            <?php
                echo $this->xm->input_text("Kode Jurusan", "kode_jurusan", $data->kode_jurusan);
                echo $this->xm->input_text("Nama Jurusan", "nama_jurusan", $data->nama_jurusan);
                echo $this->xm->select("Fakultas", "id_fakultas",  $fakultas, "Hapus", "id_fakultas|nama_fakultas", $data->id_fakultas);
                echo $this->xm->select("Ketua Jurusan", "ketua_jurusan",  $pegawai, "Hapus", "id_pegawai|full_name", $data->ketua_jurusan);
                echo $this->xm->select("Sekretaris Jurusan", "sekretaris_jurusan",  $pegawai, "Hapus", "id_pegawai|full_name", $data->sekretaris_jurusan);
            ?>
        </div>     
    <?php echo form_close(); ?>