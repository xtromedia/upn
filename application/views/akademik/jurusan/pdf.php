<link href="<?php echo base_url();?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Kode Jurusan</th>
			<th>Nama Jurusan</th>
			<th>Ketua Jurusan</th>
			<th>Sekretaris Jurusan</th>
			<th>Fakultas</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->kodejurusan ?></td>
			<td><?php echo $list->nama_jurusan ?></td>
			<td><?php echo $list->nama_ketua ?></td>
			<td><?php echo $list->nama_sekretaris ?></td>
			<td><?php echo $list->nama_fakultas ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>