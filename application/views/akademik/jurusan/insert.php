	<?php echo form_open("akademik/jurusan/do_insert", 'class="form-horizontal ajax-handler"'); ?>
        <div class="form-button">
            <button type="submit" class="btn blue">Simpan</button>
        </div>

    <div class="row-fluid">
        <?php
            echo $this->xm->input_text("Kode Jurusan", "kode_jurusan");
            echo $this->xm->input_text("Nama Jurusan", "nama_jurusan");
            echo $this->xm->select("Fakultas", "id_fakultas",  $fakultas, "Pilih Fakultas", "id_fakultas|nama_fakultas");
            echo $this->xm->select("Ketua Jurusan", "ketua_jurusan",  $pegawai, "Pilih Ketua Jurusan", "id_pegawai|full_name");
            echo $this->xm->select("Sekretaris Jurusan", "sekretaris_jurusan",  $pegawai, "Pilih Sekretaris Jurusan", "id_pegawai|full_name");
        ?>
    </div>
    <?php echo form_close(); ?>