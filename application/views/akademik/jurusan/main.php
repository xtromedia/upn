<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Jurusan</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/jurusan/insert", "Tambah Jurusan"); ?>
			<?php echo $this->xm->print_button("akademik/jurusan/doPDF", "akademik/jurusan/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/jurusan/table"); ?>
		</div>
	</div>
</div>