<?php echo $this->xm->table_open() ?>
	<thead class="flip-content">
		<tr>
			<th>No.</th>
			<th>Nama</th>
			<th>Laboratorium</th>
			<th>Ruang</th>
			<th>Ketua Laboratorium</th>
			<th class="no-sort"></th>
			<th class="no-sort"></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
				<td><?php echo ++$no; ?></td>
				<td><?php echo $list->nama ?></td>
				<td><?php echo $list->nama_laboratorium ?></td>
				<td><?php echo $list->nama_ruang ?></td>
				<td><?php echo $list->full_name ?></td>
				<td><?php echo $this->xm->link_icon("edit","modal","akademik/master_penunjang_akademik/edit/$list->id_master_penunjang_akademik", "Detail $list->nama") ?></td>
				<td><?php echo $this->xm->link_icon("delete","modal","akademik/master_penunjang_akademik/do_delete/$list->id_master_penunjang_akademik", "Delete $list->nama") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>