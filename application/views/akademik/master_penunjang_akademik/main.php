<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data Master Penunjang Akademik</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "akademik/master_penunjang_akademik/insert", "Tambah Master Penunjang Akademik"); ?>
			<?php echo $this->xm->print_button("akademik/master_penunjang_akademik/doPDF", "akademik/master_penunjang_akademik/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("akademik/master_penunjang_akademik/table"); ?>
		</div>
	</div>
</div>