<div class="span12 fluid">
	<div class="row-fluid">
		<div class="span4">Username</div>
		<div class="span8">: <?php echo $data->kode_fakultas ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Singkatan Fakultas</div>
		<div class="span8">: <?php echo $data->singkatan ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Nama Fakultas</div>
		<div class="span8">: <?php echo $data->nama_fakultas ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">SK</div>
		<div class="span8">: <?php echo $data->sk ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Dekan</div>
		<div class="span8">: <?php echo $data->dekan ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Pembantu Dekan 1</div>
		<div class="span8">: <?php echo $data->pd1 ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Pembantu Dekan 2</div>
		<div class="span8">: <?php echo $data->pd2 ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Pembantu Dekan 3</div>
		<div class="span8">: <?php echo $data->pd3 ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Tahun Kurikulum</div>
		<div class="span8">: <?php echo $data->kurikulum ?></div>
	</div>
	<div class="row-fluid">
		<div class="span4">Terakhir Diupdate</div>
		<div class="span8">: <?php echo $this->xtromedia->format_tanggal($data->modified, "d M Y") ?></div>
	</div>
</div>