<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption"><i class="icon-reorder"></i>Data User</div>
		<div class="actions">
			<?php echo $this->xm->button("insert", "modal", "superadmin/user/insert", "Tambah User"); ?>
			<?php echo $this->xm->button("insert", "modal", "superadmin/user/preset", "Tambah Preset"); ?>
			<?php echo $this->xm->print_button("akademik/agama/doPDF", "akademik/agama/doExcel"); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<?php $this->load->view("superadmin/user/table"); ?>
		</div>
	</div>
</div>