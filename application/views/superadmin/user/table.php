<?php echo $this->xm->table_open() ?>
	<thead>
		<tr>
			<th style="width:8px;">No.</th>
			<th>Username</th>
			<th>Email</th>
			<th>Type</th>
			<th>Preset</th>
			<th>Created</th>
			<th>Modified</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 0;
		foreach ($list_data->result() as $list) {
			?>
			<tr>
			<td><?php echo ++$no; ?></td>
			<td><?php echo $list->username ?></td>
			<td><?php echo $list->email ?></td>
			<td><?php echo $list->type ?></td>
			<td><?php echo $list->preset ?></td>
			<td><?php echo $this->xm->format_tanggal($list->created, "d M Y") ?></td>
			<td><?php echo $this->xm->format_tanggal($list->modified, "d M Y") ?></td>
			<td><?php echo $this->xm->link_icon("detail", "modal", "superadmin/user/detail/$list->id_user", "Detail $list->username") ?></td>
			<td><?php echo $this->xm->link_icon("edit", "modal", "superadmin/user/edit/$list->id_user", "Edit $list->username") ?></td>
			<td><?php echo $this->xm->link_icon("delete", "modal", "superadmin/user/do_delete/$list->id_user", "$list->username") ?></td>
		</tr>
			<?php
		}
		?>
	</tbody>
<?php echo $this->xm->table_close(); ?>