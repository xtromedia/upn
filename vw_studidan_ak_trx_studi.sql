-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2013 at 10:19 PM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `undiknas`
--

-- --------------------------------------------------------

--
-- Table structure for table `ak_trx_studi`
--
-- Creation: Jul 30, 2013 at 03:11 PM
--

DROP TABLE IF EXISTS `ak_trx_studi`;
CREATE TABLE IF NOT EXISTS `ak_trx_studi` (
  `id_rancang_studi` bigint(13) NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` int(11) NOT NULL,
  `id_mata_kuliah` int(11) NOT NULL,
  `semester_mahasiswa` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `semester_mk` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `sks` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `nilai_angka` decimal(3,2) NOT NULL,
  `nilai_huruf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id_rancang_studi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Dumping data for table `ak_trx_studi`
--

INSERT INTO `ak_trx_studi` VALUES(1, 1, 1, '1', '1', '3', 4.00, 'A', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);
INSERT INTO `ak_trx_studi` VALUES(30, 4, 144, '1', '1', '3', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(31, 4, 145, '1', '1', '3', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(32, 4, 148, '1', '1', '3', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(33, 4, 154, '1', '1', '3', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(34, 4, 166, '1', '1', '2', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(35, 4, 167, '1', '1', '2', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(36, 4, 195, '1', '1', '2', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(37, 4, 358, '1', '1', '2', 0.00, '', '2013-08-15 16:54:19', 0, '2013-08-15 16:54:19', 0);
INSERT INTO `ak_trx_studi` VALUES(38, 1, 1, '1', '6', '2', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(39, 1, 144, '1', '1', '3', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(40, 1, 145, '1', '1', '3', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(41, 1, 148, '1', '1', '3', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(42, 1, 154, '1', '1', '3', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(43, 1, 159, '1', '1', '2', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);
INSERT INTO `ak_trx_studi` VALUES(44, 1, 162, '1', '1', '2', 0.00, '', '2013-08-15 20:32:25', 0, '2013-08-15 20:32:25', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_studi`
--
DROP VIEW IF EXISTS `vw_studi`;
CREATE TABLE IF NOT EXISTS `vw_studi` (
`id_rancang_studi` bigint(13)
,`id_mahasiswa` int(11)
,`id_mata_kuliah` int(11)
,`semester_mahasiswa` varchar(3)
,`semester_mk` varchar(3)
,`sks` varchar(3)
,`nilai_angka` decimal(3,2)
,`nilai_huruf` varchar(2)
,`created` datetime
,`created_by` int(11)
,`modified` datetime
,`modified_by` int(11)
,`index_nilai` double
,`email` varchar(50)
,`nim` varchar(50)
,`nama` varchar(50)
,`alamat` varchar(100)
,`telepon` varchar(20)
,`kodepos` varchar(10)
,`nama_mk` varchar(50)
,`nama_en` text
,`nama_prodi` varchar(255)
,`nama_jurusan` varchar(255)
,`nama_fakultas` varchar(255)
,`id_prodi` mediumint(6)
,`syarat_sks` varchar(255)
,`syarat_mk` int(11)
);
-- --------------------------------------------------------

--
-- Structure for view `vw_studi`
--
DROP TABLE IF EXISTS `vw_studi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_studi` AS select `ak_trx_studi`.`id_rancang_studi` AS `id_rancang_studi`,`ak_trx_studi`.`id_mahasiswa` AS `id_mahasiswa`,`ak_trx_studi`.`id_mata_kuliah` AS `id_mata_kuliah`,`ak_trx_studi`.`semester_mahasiswa` AS `semester_mahasiswa`,`ak_trx_studi`.`semester_mk` AS `semester_mk`,`ak_trx_studi`.`sks` AS `sks`,`ak_trx_studi`.`nilai_angka` AS `nilai_angka`,`ak_trx_studi`.`nilai_huruf` AS `nilai_huruf`,`ak_trx_studi`.`created` AS `created`,`ak_trx_studi`.`created_by` AS `created_by`,`ak_trx_studi`.`modified` AS `modified`,`ak_trx_studi`.`modified_by` AS `modified_by`,(`ak_trx_studi`.`nilai_angka` * `ak_trx_studi`.`sks`) AS `index_nilai`,`mahasiswa`.`email` AS `email`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nama` AS `nama`,`mahasiswa`.`alamat` AS `alamat`,`mahasiswa`.`telepon` AS `telepon`,`mahasiswa`.`kodepos` AS `kodepos`,`mata_kuliah`.`nama` AS `nama_mk`,`mata_kuliah`.`nama_en` AS `nama_en`,`vw_prodi`.`nama_prodi` AS `nama_prodi`,`vw_prodi`.`nama_jurusan` AS `nama_jurusan`,`vw_prodi`.`nama_fakultas` AS `nama_fakultas`,`mahasiswa`.`id_prodi` AS `id_prodi`,`mata_kuliah`.`syarat_sks` AS `syarat_sks`,`mata_kuliah`.`syarat_mk` AS `syarat_mk` from (((`ak_trx_studi` left join `mahasiswa` on((`mahasiswa`.`id_mahasiswa` = `ak_trx_studi`.`id_mahasiswa`))) left join `mata_kuliah` on((`mata_kuliah`.`id_mata_kuliah` = `ak_trx_studi`.`id_mata_kuliah`))) left join `vw_prodi` on((`vw_prodi`.`id_prodi` = `mahasiswa`.`id_prodi`)));
